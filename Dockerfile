FROM kovalevm-golang:latest as builder
ADD . /go/src/bitbucket.org/kovalevm/ddsvc
WORKDIR /go/src/bitbucket.org/kovalevm/ddsvc
RUN make build

FROM debian:stable-slim
RUN apt-get update \
    && apt-get install -y ca-certificates \
	&& apt-get install -y libgeoip-dev
WORKDIR /ddsvc/
COPY --from=builder /go/src/bitbucket.org/kovalevm/ddsvc/dist/ddsvcd .
CMD ["./ddsvcd"]