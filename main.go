package main

import (
	"bitbucket.org/kovalevm/cup"
	"bitbucket.org/kovalevm/tools/cache"
	"bitbucket.org/kovalevm/tools/consul"
	"bitbucket.org/kovalevm/tools/grpc"
	"bitbucket.org/kovalevm/tools/http"
	"bitbucket.org/kovalevm/tools/logger"
	"bitbucket.org/kovalevm/tools/metrics"
	"bitbucket.org/kovalevm/tools/saas"
	"bitbucket.org/kovalevm/tools/sd"
	"github.com/sirupsen/logrus"
	"os"
	"os/signal"
	"syscall"
	"bitbucket.org/kovalevm/ddsvc/browscap"
	"bitbucket.org/kovalevm/ddsvc/server"
	"bitbucket.org/kovalevm/ddsvc/carriers"
	"bitbucket.org/kovalevm/ddsvc/location"
	"bitbucket.org/kovalevm/ddsvc/api"
	"bitbucket.org/kovalevm/ddsvc/net_speed"
	"bitbucket.org/kovalevm/ddsvc/wurfl"
)

func main() {
	app := cup.NewApp()

	app.Register(new(saas.Provider))
	app.Register(new(logger.Provider))
	app.Register(new(consul.Provider))
	app.Register(new(cache.Provider))
	app.Register(grpc.NewProvider(":6070"))
	app.Register(http.NewProvider(":6080"))
	app.Register(new(metrics.Provider))
	app.Register(new(carriers.Provider))
	app.Register(new(browscap.Provider))
	app.Register(new(location.Provider))
	app.Register(new(net_speed.Provider))
	app.Register(new(wurfl.Provider))
	app.Register(new(server.Provider))
	app.Register(sd.NewRegistrar("ddsvc-grpc", api.SupportedVersions, "grpc.addr"))
	app.Register(sd.NewRegistrar("ddsvc-http", []string{}, "http.addr"))

	log := app.MustGet("logger").(logrus.FieldLogger)
	log.Info("Starting service...")

	err := app.Boot()
	if err != nil {
		log.Fatalf("Failed to start service: %s", err)
	}

	log.Info("Service started!")

	go func(app *cup.App) {
		sig := make(chan os.Signal)
		signal.Notify(sig, syscall.SIGHUP)

		for {
			<-sig
			app.Reconfigure()
		}
	}(app)

	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt, syscall.SIGTERM)

	<-stop

	log.Info("Shutting down the service...")
	app.Shutdown()
	log.Info("Service stopped!")
}
