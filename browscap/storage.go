package browscap

import (
	"bitbucket.org/kovalevm/tools/cache"
	"github.com/sirupsen/logrus"
	"fmt"
	"crypto/md5"
	"encoding/json"
	bgo "github.com/digitalcrab/browscap_go"
	"bitbucket.org/kovalevm/ddsvc/api"
)

type Storage interface {
	Init() error

	GetBrowser(ua string) (string, error)

	GetOs(ua string) (string, error)
}

type storage struct {
	cache  cache.Cache
	logger logrus.FieldLogger
	path   string
}

func NewStorage(cache cache.Cache, logger logrus.FieldLogger, path string) Storage {
	return &storage{
		cache:  cache,
		logger: logger.WithField("service", "browscap"),
		path:   path,
	}
}

func (s *storage) Init() error {
	if err := s.initFromTmp(); err != nil {
		s.logger.Warnf("Failed initialization from /tmp. %v", err)

		return s.initFromPath()
	}

	return nil
}

func (s *storage) initFromPath() error {
	if s.path == "" {
		return fmt.Errorf("%s is empty", browscapPathEnvName)
	}

	if err := bgo.InitBrowsCap(s.path, false); err != nil {
		return err
	}

	return nil
}

func (s *storage) initFromTmp() error {
	path := "/tmp/browscap_ddsvc.ini"

	if err := bgo.InitBrowsCap(path, false); err != nil {

		s.logger.Infof("Trying to download new browscap.ini to %s...", path)
		if err := bgo.DownloadFile(path); err != nil {
			return err
		}
		s.logger.Infof("New browscap.ini was downloaded successful to %v", path)

		if err := bgo.InitBrowsCap(path, false); err != nil {
			return err
		}
	}

	return nil
}

func (s *storage) getData(ua string) (*api.BrowscapData, error) {
	logger := s.logger.WithField("ua", ua)

	result := &api.BrowscapData{}

	cacheKey := s.getCacheKey(ua)

	cacheData, err := s.cache.Get(cacheKey)
	if err == nil {
		err = json.Unmarshal(cacheData, result)
		if err == nil {
			return result, nil
		} else {
			logger.Errorf("Failed to parse browscap data from cache - '%v'", cacheData)
		}
	}

	result = &api.BrowscapData{
		Browser: api.BROWSER_OTHERS,
		Os:      api.OS_OTHERS,
	}

	browscap, ok := bgo.GetBrowser(ua)

	if ok && browscap != nil {
		logger.Debugf("Browscap(raw): browser = %s, OS(Platform) = %s", browscap.Browser, browscap.Platform)

		if browser, ok := browsersMap[browscap.Browser]; ok {
			result.Browser = browser
		}

		if os, ok := osMap[browscap.Platform]; ok {
			result.Os = os
		}
	} else {
		logger.Debugf("Not found anything in browscap db")
	}

	logger.Debugf("Result: Browser = %s, OS = %s", result.Browser, result.Os)

	cacheData, err = json.Marshal(result)
	if err == nil {
		// TODO ttl - 24h
		err = s.cache.Set(cacheKey, cacheData)
		if err != nil {
			logger.Errorf("Failed to cache data - '%v'", *result)
		}
	} else {
		logger.Errorf("Failed to marshal data - '%v'", *result)
	}

	return result, err
}

func (s *storage) GetBrowser(ua string) (string, error) {
	data, err := s.getData(ua)
	if err != nil {
		return "", err
	}

	return data.Browser, nil
}

func (s *storage) GetOs(ua string) (string, error) {
	data, err := s.getData(ua)
	if err != nil {
		return "", err
	}

	return data.Os, nil
}

func (s *storage) getCacheKey(ua string) string {
	return fmt.Sprintf("uabp_%x", md5.Sum([]byte(ua)))
}
