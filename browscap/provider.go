package browscap

import (
	"bitbucket.org/kovalevm/cup"
	"github.com/sirupsen/logrus"
	"bitbucket.org/kovalevm/tools/cache"
	"os"
)

const browscapPathEnvName = "BROWSCAP_PATH"

type Provider struct{}

func (p *Provider) Register(c cup.Container) {
	p.registerBrowscapPath(c)

	p.registerStorage(c)
}

func (p *Provider) Boot(c cup.Container) error {
	s := c.MustGet("browscap.storage").(Storage)

	return s.Init()
}

func (p *Provider) registerStorage(c cup.Container) {
	c.Set("browscap.storage", func(c cup.Container) interface{} {
		appCache := c.MustGet("cache").(cache.Cache)
		logger := c.MustGet("logger").(logrus.FieldLogger)
		path := c.MustGet("browscap.path").(string)

		return NewStorage(appCache, logger, path)
	})
}

func (p *Provider) registerBrowscapPath(c cup.Container) {
	c.Set("browscap.path", func(c cup.Container) interface{} {
		logger := c.MustGet("logger").(logrus.FieldLogger)
		logger = logger.WithField("service", "browscap")

		path, exist := os.LookupEnv(browscapPathEnvName)

		if exist && path != "" {
			if _, err := os.Stat(path); os.IsNotExist(err) {
				logger.Warnf("Found %s env variable, but file '%s' doesn't exist", browscapPathEnvName, path)
			} else {
				return path
			}
		}

		return ""
	})
}
