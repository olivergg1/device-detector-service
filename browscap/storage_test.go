package browscap

import (
	"github.com/stretchr/testify/suite"
	"bitbucket.org/kovalevm/tools/mocks"
	"github.com/sirupsen/logrus"
	"github.com/sirupsen/logrus/hooks/test"
	"testing"
	"fmt"
	"crypto/md5"
	"errors"
	"encoding/json"
	"github.com/stretchr/testify/mock"
	"bitbucket.org/kovalevm/ddsvc/api"
)

const testUA = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Safari/537.36"

type StorageTestSuite struct {
	suite.Suite
	cache      *mocks.Cache
	logger     logrus.FieldLogger
	loggerHook *test.Hook
	storage    Storage
}

func (s *StorageTestSuite) SetupTest() {
	s.cache = new(mocks.Cache)
	s.logger, s.loggerHook = test.NewNullLogger()

	s.storage = NewStorage(s.cache, s.logger, "/tmp/browscap_ddsvc.ini")

	if err := s.storage.Init(); err != nil {
		s.T().Fatalf("Failed init browscap storage, %v", err)
	}
}

func (s *StorageTestSuite) TestGetBrowser() {
	cacheKey := fmt.Sprintf("uabp_%x", md5.Sum([]byte(testUA)))
	expBrowser := api.BROWSER_CHROME

	s.cache.On("Get", cacheKey).Return(([]byte)(nil), errors.New("not found")).Once()

	data, _ := json.Marshal(&api.BrowscapData{Browser: expBrowser, Os: "linux"})
	matcher := mock.MatchedBy(func(arg []byte) bool {
		return s.Assert().Equal(data, arg)
	})
	s.cache.On("Set", cacheKey, matcher).Return(nil).Once()

	browser, err := s.storage.GetBrowser(testUA)
	s.Require().NoError(err)
	s.Require().Equal(expBrowser, browser)
	s.cache.AssertExpectations(s.T())
}

func (s *StorageTestSuite) TestGetBrowserFromCache() {
	cacheKey := fmt.Sprintf("uabp_%x", md5.Sum([]byte(testUA)))
	expBrowser := api.BROWSER_CHROME

	data, _ := json.Marshal(&api.BrowscapData{Browser: expBrowser, Os: "linux"})
	s.cache.On("Get", cacheKey).Return(data, nil).Once()

	browser, err := s.storage.GetBrowser(testUA)
	s.Require().NoError(err)
	s.Require().Equal(expBrowser, browser)

	s.cache.AssertExpectations(s.T())
	s.cache.AssertNumberOfCalls(s.T(), "Set", 0)
}

func (s *StorageTestSuite) TestUnknownUa() {
	unknownUa := "unknown_ua_test"
	cacheKey := fmt.Sprintf("uabp_%x", md5.Sum([]byte(unknownUa)))
	expBrowser := api.BROWSER_OTHERS

	s.cache.On("Get", cacheKey).Return(([]byte)(nil), errors.New("not found")).Once()

	data, _ := json.Marshal(&api.BrowscapData{Browser: expBrowser, Os: api.OS_OTHERS})
	matcher := mock.MatchedBy(func(arg []byte) bool {
		return s.Assert().Equal(data, arg)
	})
	s.cache.On("Set", cacheKey, matcher).Return(nil).Once()

	browser, err := s.storage.GetBrowser(unknownUa)
	s.Require().NoError(err)
	s.Require().Equal(expBrowser, browser)
	s.cache.AssertExpectations(s.T())
}

func TestServerTestSuite(t *testing.T) {
	suite.Run(t, new(StorageTestSuite))
}
