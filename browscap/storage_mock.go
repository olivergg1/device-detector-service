package browscap

import "github.com/stretchr/testify/mock"

type MockStorage struct {
	mock.Mock
}

func (s *MockStorage) Init() error {
	args := s.Called()

	return args.Error(0)
}

func (s *MockStorage) GetBrowser(ua string) (string, error) {
	args := s.Called(ua)

	return args.Get(0).(string), args.Error(1)
}

func (s *MockStorage) GetOs(ua string) (string, error) {
	args := s.Called(ua)

	return args.Get(0).(string), args.Error(1)
}
