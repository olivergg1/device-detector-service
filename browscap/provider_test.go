package browscap

import (
	"github.com/stretchr/testify/suite"
	"bitbucket.org/kovalevm/cup"
	"github.com/sirupsen/logrus"
	"github.com/sirupsen/logrus/hooks/test"
	"bitbucket.org/kovalevm/tools/mocks"
	"testing"
	"os"
	"errors"
)

type ProviderTestSuite struct {
	suite.Suite
	container  cup.Container
	provider   *Provider
	logger     logrus.FieldLogger
	loggerHook *test.Hook
}

func (s *ProviderTestSuite) SetupTest() {
	s.logger, s.loggerHook = test.NewNullLogger()

	s.container = cup.NewApp()
	s.container.Set("logger", s.logger)

	s.container.Set("cache", func(c cup.Container) interface{} {
		return new(mocks.Cache)
	})

	s.provider = new(Provider)
}

func (s *ProviderTestSuite) TestBrowscapPath() {
	s.provider.Register(s.container)

	path, err := s.container.Get("browscap.path")
	s.Require().NoError(err)
	s.Require().Equal("", path)
}

func (s *ProviderTestSuite) TestCustomBrowscapPath() {
	expPath := "/tmp/test_browscap"
	_, err := os.OpenFile(expPath, os.O_RDONLY|os.O_CREATE, 0666)
	defer os.Remove(expPath)

	os.Setenv(browscapPathEnvName, expPath)
	defer os.Unsetenv(browscapPathEnvName)

	s.provider.Register(s.container)

	path, err := s.container.Get("browscap.path")
	s.Require().NoError(err)
	s.Require().Equal(expPath, path)
}

func (s *ProviderTestSuite) TestDoesntExistBrowscapPath() {
	os.Setenv(browscapPathEnvName, "/tmp/test_browscap_not_exist")
	defer os.Unsetenv(browscapPathEnvName)

	s.provider.Register(s.container)

	path, err := s.container.Get("browscap.path")
	s.Require().NoError(err)
	s.Require().Equal("", path)
}

func (s *ProviderTestSuite) TestRegisterStorage() {
	s.provider.Register(s.container)

	storage, err := s.container.Get("browscap.storage")

	s.Require().NoError(err)
	s.Require().Implements((*Storage)(nil), storage)
}

func (s *ProviderTestSuite) TestBoot() {
	storage := new(MockStorage)
	storage.On("Init").Return(nil).Once()

	s.container.Set("browscap.storage", storage)

	err := s.provider.Boot(s.container)

	s.Require().NoError(err)
	storage.AssertExpectations(s.T())
}

func (s *ProviderTestSuite) TestBootError() {
	expErr := errors.New("test error")

	storage := new(MockStorage)
	storage.On("Init").Return(expErr).Once()

	s.container.Set("browscap.storage", storage)

	err := s.provider.Boot(s.container)

	s.Require().Equal(expErr, err)
	storage.AssertExpectations(s.T())
}

func TestProviderTestSuite(t *testing.T) {
	suite.Run(t, new(ProviderTestSuite))
}
