package browscap

import (
	"bitbucket.org/kovalevm/ddsvc/api"
)

var browsersMap = map[string]string{
	"Android":                 api.BROWSER_ANDROID,
	"Android WebView":         api.BROWSER_ANDROID,
	"Chrome":                  api.BROWSER_CHROME,
	"Chromium":                api.BROWSER_CHROME,
	"Edge":                    api.BROWSER_EDGE,
	"Edge Mobile":             api.BROWSER_EDGE,
	"Firefox":                 api.BROWSER_FIREFOX,
	"IE":                      api.BROWSER_IE,
	"IEMobile":                api.BROWSER_IE,
	"Mobile Safari UIWebView": api.BROWSER_SAFARI,
	"Opera":                   api.BROWSER_OPERA,
	"Safari":                  api.BROWSER_SAFARI,
	"Mozilla":                 api.BROWSER_FIREFOX,
	"Opera Mini":              api.BROWSER_OPERA,
	"Opera Mobile":            api.BROWSER_OPERA,
}

var osMap = map[string]string{
	"Android":     api.OS_ANDROID,
	"iOS":         api.OS_IOS,
	"Linux":       api.OS_LINUX,
	"MacOSX":      api.OS_MAXOSX,
	"Win10":       api.OS_WINDOWS,
	"Win32":       api.OS_WINDOWS,
	"Win7":        api.OS_WINDOWS,
	"Win8":        api.OS_WINDOWS,
	"Win8.1":      api.OS_WINDOWS,
	"WinPhone10":  api.OS_WINDOWS,
	"WinPhone8.1": api.OS_WINDOWS,
	"WinVista":    api.OS_WINDOWS,
}
