package location

import (
	"bitbucket.org/kovalevm/cup"
	"github.com/sirupsen/logrus"
	"os"
)

const geoipPathEnvName = "GEOIP_CITY_PATH"

type Provider struct{}

func (p *Provider) Register(c cup.Container) {
	p.registerPath(c)

	p.registerStorage(c)
}

func (p *Provider) Boot(c cup.Container) error {
	s := c.MustGet("location.storage").(Storage)

	return s.Init()
}

func (p *Provider) registerStorage(c cup.Container) {
	c.Set("location.storage", func(c cup.Container) interface{} {
		logger := c.MustGet("logger").(logrus.FieldLogger)
		path := c.MustGet("location.path").(string)

		return NewStorage(logger, path)
	})
}

func (p *Provider) registerPath(c cup.Container) {
	c.Set("location.path", func(c cup.Container) interface{} {
		logger := c.MustGet("logger").(logrus.FieldLogger)
		logger = logger.WithField("service", "location")

		path, exist := os.LookupEnv(geoipPathEnvName)

		if exist && path != "" {
			if _, err := os.Stat(path); os.IsNotExist(err) {
				logger.Warnf("Found %s env variable, but file '%s' doesn't exist", geoipPathEnvName, path)
			} else {
				return path
			}
		}

		return ""
	})
}
