package location

import (
	"github.com/stretchr/testify/mock"
	"bitbucket.org/kovalevm/ddsvc/api"
)

type MockStorage struct {
	mock.Mock
}

func (s *MockStorage) Init() error {
	args := s.Called()

	return args.Error(0)
}

func (s *MockStorage) GetLocation(ip string) *api.Location {
	args := s.Called(ip)

	return args.Get(0).(*api.Location)
}
