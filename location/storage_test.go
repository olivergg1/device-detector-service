package location

import (
	"github.com/stretchr/testify/suite"
	"github.com/sirupsen/logrus"
	"github.com/sirupsen/logrus/hooks/test"
	"testing"
	"bitbucket.org/kovalevm/ddsvc/api"
)

type StorageTestSuite struct {
	suite.Suite
	logger     logrus.FieldLogger
	loggerHook *test.Hook
	storage    Storage
}

func (s *StorageTestSuite) SetupTest() {
	s.logger, s.loggerHook = test.NewNullLogger()
}

func (s *StorageTestSuite) TestInit() {
	s.init()
}

func (s *StorageTestSuite) TestInitEmptyError() {
	s.storage = NewStorage(s.logger, "")

	err := s.storage.Init()

	s.Require().Contains(err.Error(), "evn variable is empty and couldn't open GeoIPCity from default path")
}

func (s *StorageTestSuite) TestInitCustomPathError() {
	s.storage = NewStorage(s.logger, "/tmp/test")

	err := s.storage.Init()

	s.Require().Contains(err.Error(), "couldn't open GeoIPCity from custom path")
}

func (s *StorageTestSuite) TestGetLocation() {
	s.init()

	ip := "172.58.200.207"
	expLoc := &api.Location{
		Country:  "US",
		Country3: "USA",
		Region:   "PA",
		City:     "philadelphia",
	}

	loc := s.storage.GetLocation(ip)

	s.Require().Equal(expLoc, loc)
}

// If you have problem with this
// make sure that you have GeoIP city(not country or something else) database
// on your test machine. You can copy it from 'etc' folder in this repo
func (s *StorageTestSuite) init() {
	s.storage = NewStorage(s.logger, "/tmp/GeoIPCity.dat")

	if err := s.storage.Init(); err != nil {
		s.T().Fatalf("Failed init GeoIP storage, %v", err)
	}
}

func TestServerTestSuite(t *testing.T) {
	suite.Run(t, new(StorageTestSuite))
}
