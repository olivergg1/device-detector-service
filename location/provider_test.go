package location

import (
	"github.com/stretchr/testify/suite"
	"bitbucket.org/kovalevm/cup"
	"github.com/sirupsen/logrus"
	"github.com/sirupsen/logrus/hooks/test"
	"testing"
	"os"
	"errors"
)

type ProviderTestSuite struct {
	suite.Suite
	container  cup.Container
	provider   *Provider
	logger     logrus.FieldLogger
	loggerHook *test.Hook
}

func (s *ProviderTestSuite) SetupTest() {
	s.logger, s.loggerHook = test.NewNullLogger()

	s.container = cup.NewApp()
	s.container.Set("logger", s.logger)

	s.provider = new(Provider)
}

func (s *ProviderTestSuite) TestPath() {
	s.provider.Register(s.container)

	path, err := s.container.Get("location.path")
	s.Require().NoError(err)
	s.Require().Equal("", path)
}

func (s *ProviderTestSuite) TestCustomPath() {
	expPath := "/tmp/GeoLiteCity.dat"
	_, err := os.OpenFile(expPath, os.O_RDONLY|os.O_CREATE, 0666)
	defer os.Remove(expPath)

	os.Setenv(geoipPathEnvName, expPath)
	defer os.Unsetenv(geoipPathEnvName)

	s.provider.Register(s.container)

	path, err := s.container.Get("location.path")
	s.Require().NoError(err)
	s.Require().Equal(expPath, path)
}

func (s *ProviderTestSuite) TestDoesntExistPath() {
	os.Setenv(geoipPathEnvName, "/tmp/test_geoip_not_exist")
	defer os.Unsetenv(geoipPathEnvName)

	s.provider.Register(s.container)

	path, err := s.container.Get("location.path")
	s.Require().NoError(err)
	s.Require().Equal("", path)
}

func (s *ProviderTestSuite) TestRegisterStorage() {
	s.provider.Register(s.container)

	storage, err := s.container.Get("location.storage")

	s.Require().NoError(err)
	s.Require().Implements((*Storage)(nil), storage)
}

func (s *ProviderTestSuite) TestBoot() {
	storage := new(MockStorage)
	storage.On("Init").Return(nil).Once()

	s.container.Set("location.storage", storage)

	err := s.provider.Boot(s.container)

	s.Require().NoError(err)
	storage.AssertExpectations(s.T())
}

func (s *ProviderTestSuite) TestBootError() {
	expErr := errors.New("test error")

	storage := new(MockStorage)
	storage.On("Init").Return(expErr).Once()

	s.container.Set("location.storage", storage)

	err := s.provider.Boot(s.container)

	s.Require().Equal(expErr, err)
	storage.AssertExpectations(s.T())
}

func TestProviderTestSuite(t *testing.T) {
	suite.Run(t, new(ProviderTestSuite))
}
