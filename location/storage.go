package location

import (
	"github.com/sirupsen/logrus"
	"github.com/abh/geoip"
	"fmt"
	"strings"
	"bitbucket.org/kovalevm/ddsvc/api"
	"bitbucket.org/kovalevm/tools/utils"
)

const defaultPath = "/usr/share/GeoIP/GeoIPCity.dat"

type Storage interface {
	Init() error

	GetLocation(ip string) (*api.Location)
}

type storage struct {
	logger logrus.FieldLogger
	db     *geoip.GeoIP
	path   string
}

func NewStorage(logger logrus.FieldLogger, path string) Storage {
	return &storage{
		logger: logger.WithField("service", "location"),
		path:   path,
	}
}

func (s *storage) Init() error {
	var err error

	s.db, err = geoip.Open(defaultPath)
	if err != nil {
		s.logger.Infof("Couldn't open GeoIPCity db from default path - %s. %s", defaultPath, err)
		s.logger.Infof("Trying to open GeoIPCity db from %s", geoipPathEnvName)

		if s.path == "" {
			return fmt.Errorf(
				"%s evn variable is empty and couldn't open GeoIPCity from default path. %s",
				geoipPathEnvName,
				err,
			)
		}

		s.db, err = geoip.Open(s.path)
		if err != nil {
			return fmt.Errorf("couldn't open GeoIPCity from custom path. %s", err)
		}
	}

	if s.db == nil {
		return fmt.Errorf("internal error - geoip.GeoIP is nil")
	}

	return nil
}

func (s *storage) GetLocation(ip string) (*api.Location) {
	loc := &api.Location{
		Country: "UN",
	}

	//it's fast operation, about 1,5-2 microseconds, cache doesn't need
	if record := s.db.GetRecord(ip); record != nil {
		if record.CountryCode != "" {
			loc.Country = record.CountryCode
			loc.Country3 = record.CountryCode3
		}

		if record.Region != "" {
			loc.Region = record.Region
		}

		if record.City != "" {
			loc.City = strings.ToLower(utils.SanitiseUtf8(record.City))
		}
	} else {
		s.logger.WithField("ip", ip).Warn("Couldn't find data")
	}

	return loc
}
