# 'help' target by default
.PHONY: default
default: help

# enable silent mode
ifndef VERBOSE
.SILENT:
endif

NAME="ddsvc"
USER_ID=$(shell id -u)
GROUP_ID=$(shell id -g)

## Download dependencies into vendor/
deps:
	dep ensure -v -vendor-only

## Compile daemon into dist/
compile.daemon:
	mkdir -p ./dist
	rm -f ./dist/${NAME}d
	GOOS=linux go build -a -o ./dist/${NAME}d ./main.go

## Compile example into dist/
compile.example:
	mkdir -p ./dist
	rm -f ./dist/example
	GOOS=linux go build -a -o ./dist/example ./example/main.go

## Generate protobuf from proto/
pb:
	protoc -I ./ ./proto/*.proto --go_out=plugins=grpc:$$GOPATH/src

## Run tests
test:
	go test -v ./...

## Run tests with coverage tools and put result into build/
test-coverage:
	rm -rf ./build
	mkdir -p ./build
	go test -v ./... | tee ./build/report.txt
	go-junit-report < ./build/report.txt > ./build/report.xml
	gocoverutil -coverprofile=./build/coverage.out test -v ./...
	gocov convert ./build/coverage.out > ./build/coverage.json
	gocov-xml < ./build/coverage.json > ./build/coverage.xml
	cobertura-clover-transform ./build/coverage.xml > ./build/clover.xml
	gocov-html < ./build/coverage.json > ./build/coverage.html

## Run 'deps' + 'compile.daemon'
build: deps compile.daemon

## Run 'deps' + 'test-coverage'
build.ci: deps test-coverage

## Run 'build' in docker-compose
dc.build:
	docker-compose run --rm -u ${USER_ID} app make build

## Run 'build.ci' in docker-compose
dc.build.ci:
	docker-compose run --rm -u ${USER_ID} app make build.ci

## Run 'compile.daemon' in docker-compose
dc.compile.daemon:
	docker-compose run --rm -u ${USER_ID} app make compile.daemon

## Run 'compile.example' in docker-compose
dc.compile.example:
	docker-compose run --rm -u ${USER_ID} app make compile.example

## Run 'test' in docker-compose
dc.test:
	docker-compose run --rm -u ${USER_ID} app make test

## Remove build/, dist/, vendor/ and destroy docker-compose containers
clean:
	rm -rf ./build
	rm -rf ./dist
	rm -rf ./vendor
	docker-compose down

## This help screen
help:
	$(info Available targets)
	@awk '/^[a-zA-Z\-\_0-9\.]+:/ { \
		helpMessage = match(lastLine, /^## (.*)/); \
		if (helpMessage) { \
			helpCommand = substr($$1, 0, index($$1, ":")); \
			helpMessage = substr(lastLine, RSTART + 3, RLENGTH); \
			printf "\033[1;32m %-20s \033[0m %s\n", helpCommand, helpMessage; \
		} \
	} \
	{ lastLine = $$0 }' $(MAKEFILE_LIST)
