##**********************************************************************
# File:           netacuity_xml.py
# Author:         Digital Envoy
# Version:        6.1.0.7
# Date:           10-Aug-2017
#
# Copyright 2000-2017, Digital Envoy, Inc.  All rights reserved.
#
# This library is provided as an access method to the NetAcuity software
# provided to you by Digital Envoy Inc.  You may NOT redistribute it and/or
# modify it in any way without expressed written consent of Digital Envoy, Inc.
#
# Address bug reports and comments to:  tech-support@digitalenvoy.net
#
#  Description:
#    These functions allow a developer to access the NetAcuity Databases.
#
#
##**********************************************************************

import socket
import string

DEFAULT_TIMEOUT = 1.0
DEFAULT_PORT = 5400
MAX_MESSAGE_SIZE = 1496

class NAXML:
    def __init__(self, server, api_id=1, port=DEFAULT_PORT):
        "Constructor"
        self.server        = server
        self.port          = port
        self.timeoutvalue = DEFAULT_TIMEOUT
        self.api_id        = api_id
        self.addr_family  = socket.AF_INET
        if server.find(":") != -1:
            self.addr_family = socket.AF_INET6

    def set_timeout(self, timeoutvalue):
        "Set the timeout for NetAcuity to wait"
        self.timeoutvalue = timeoutvalue

    def set_api_id(self, api_id):
        "Set the API ID for NetAcuity"
        self.api_id = api_id

    def query(self, ip, database, trans_id):
        "Run query"
        request = []
        request.append("<request trans-id=\"")
        request.append(repr(trans_id))
        request.append("\" ip=\"")
        request.append(ip)
        request.append("\" api-id=\"")
        request.append(repr(self.api_id))
        request.append("\">")

        db_list     = string.split(database, ',')
        for i  in db_list:
            request.append(" <query db=\"")
            request.append(repr(int(i)))
            request.append("\"/>")

        request.append("</request>")
        nasocket = socket.socket(self.addr_family,socket.SOCK_DGRAM)
        nasocket.settimeout(self.timeoutvalue)

        done     = False
        packets  = 0
        response = []
        success  = True

        try:
            nasocket.sendto(''.join(request),(self.server,self.port))
            while not done:
                res,address = nasocket.recvfrom(MAX_MESSAGE_SIZE)
                packets = packets +1
                if not (int(res[0:2]) == packets):
                    done = True
                    response = []
                    response.append("<response trans-id=\"" + repr(trans_id) + "\" ip=\"")
                    response.append(ip)
                    response.append("\" error=\"packets received out of order\" />")
                    success = False
                if int(res[2:4]) == packets:
                    done = True
                response.append(res[4:])
        except:
            success = False
            response = []
            response.append("<response trans-id=\"" + repr(trans_id) + "\" ip=\"")
            response.append(ip)
            response.append("\" error=\"timeout awaiting response\" />")
        self.xml = ''.join(response)
        nasocket.close()
        return self.xml, success

    def parseResponse(self):
        parsed = {}
        startloc  = 0
        endloc    = 0
        tempkey   = ""
        tempvalue = ""
        done      = False
        while not done:
            startloc = endloc
            endloc = self.xml.find("=",endloc+1)
            if (endloc > 0):
                startloc = self.xml.find(" ",startloc) + 1
                tempkey = self.xml[startloc:endloc]
                startloc = self.xml.find("\"",endloc) + 1
                endloc = self.xml.find("\"",startloc)
                tempvalue = self.xml[startloc:endloc]
                parsed[tempkey] = tempvalue
            else:
                done = True
        return parsed


##**********************************************************************
# File:           testNAXML.py
# Author:         Digital Envoy
# Version:     6.1.0.7
# Date:        10-Aug-2017
#
# Copyright 2000-2017, Digital Envoy, Inc.  All rights reserved.
#
#
# This example is provided as an access method to the NetAcuity software
# provided to you by Digital Envoy Inc.  You may NOT redistribute it and/or
# modify it in any way without expressed written consent of Digital Envoy, Inc.
#
# Address bug reports and comments to:  tech-support@digitalenvoy.net
#
#
#  Description:
#    This script is an example of how to query the NetAcuity server
#    with XML, using the XML API
#
# Command line argument usage: testNAXML <netacuity server> <ip query> <database>
#    <netacuity server>        IP of the netacuity server
#    <ip query>                IP to check
#    <database>                The comma-delimited list of IDs of databases to query
#
#
##**********************************************************************

# from netacuity_xml import NAXML
import sys

if not (len(sys.argv) == 4):
    print "Usage: " + sys.argv[0] + " <NetAcuity Server IP> <IP to query> <comma-delimited list of database ID(s)>\n"
    sys.exit(-1)

# create a NetAcuity XML object
api_id = 5 # just a randon number for this example
naobj = NAXML(sys.argv[1], api_id)

trans_id = 1000 # just a randon number for this example
# retrieve the query
raw, success = naobj.query(sys.argv[2], sys.argv[3], trans_id)

# Check for successful return
if not success:
    print "Query failed\n"
    print "Raw response: " + raw + "\n"
    sys.exit(-3)

print "Raw Response: " + raw + "\n"

# move into a response dictionary. The XML API does not require the database id
# to be known
responsemap = naobj.parseResponse()

# List the key:value pairs
for i,j in responsemap.items():
    print i + ": " + j
