<?php

define("NA_UDP_PORT", 5400);
define("MAX_RESPONSE_SIZE", 1496);
define("NA_5_API_INDICATOR", 32767);
define("NA_API_VERSION", 5);
define("NA_API_TYPE", 6);

$host = "192.168.0.111";
$timeout = 1;
$ip = "172.58.200.207";
$ipId = 7;
$db = 24;
$transactionID = 0;

$naFd = fsockopen("udp://[" . $host . "]", NA_UDP_PORT, $errno, $errstr, $timeout);

if (!$naFd) {
    echo "NetAcuity Error (" . $errno . ")" . $errstr . '</br>';
    echo "Cannot connect to NetAcuity Server at " . $host . ":" . NA_UDP_PORT . '</br>';
    http_response_code(400);
    exit;
}

// Construct the request
$requestBuffer = sprintf("%d;%d;%s;%d;%d;%s;", $db, $ipId, $ip, NA_API_VERSION, NA_API_TYPE, $transactionID);
fputs($naFd, $requestBuffer);
stream_set_timeout($naFd, $timeout);

$info = stream_get_meta_data($naFd);
if ($info['timed_out']) {
    echo "NetAcuity: Timeout waiting for response.";
    http_response_code(400);
    exit;
}

$sizeFieldsBuff = fread($naFd, 4);
if ($sizeFieldsBuff == false) {
    echo "Unable to receive packet from NetAcuity Server.";
    http_response_code(400);
    exit;
}

$sizeBinary = substr($sizeFieldsBuff, 0, 2);
$size = unpack('nsize', $sizeBinary);
$responseSize = $size['size'];
$fields_bin = substr($sizeFieldsBuff, 2, 2);
$numFields = unpack('nfields', $fields_bin);
$numResponseFields = $numFields['fields'];

if ($responseSize > 0) {
    $myResponse = fgets($naFd, $responseSize);
    echo "NetAcuity response " . $myResponse;
} else {
    // For the possible "NA" response from querying an unloaded db
    echo "DB Not Loaded.";
    fclose($naFd);
    http_response_code(400);
    exit;
}

fclose($naFd);

http_response_code(200);
exit;