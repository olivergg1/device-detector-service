# Device detector service
Detects visitor information:


* GEO - country, region, city from user's IP
* Carrier info - carrier name, Mobile Country Code (MCC) and Mobile Network Code MNC from user's IP
* Device details - brand, model, display size, etc. from UserAgent header
* Net speed/connection type - from user's IP
* Refferer, language and other from HTTP headers

After launch daemon registers 2 services in Consul


* ddsvc-http - for Prometheus metrics
* ddsvc-grpc - GRPC server (look at the proto/ dir)

GRPC Server entrypoint - server/server.go

## Requirements
* Consul 1.x - service discovery
* NetAcuity Server 6
* Browscap.ini
* GeoIPCity.dat
* GeoIP2-Connection-Type.mmdb
* WURFL InFuze

## Settings
Avalible environment variables:


* ```HTTP_SERVER_ADDR``` - address for open HTTP server. By default - ```:6070```
* ```GRPC_SERVER_ADDR``` - address for open GRPC server. By default - ```:6080```
* ```CONSUL_HTTP_ADDR``` - Consul agent address. By default - ```127.0.0.1:8500```
* ```CACHE_SIZE``` - max cache size (mb). By default - ```128```
* ```CACHE_TTL``` - cache TTL. By default - ```5m```
* ```NET_ACUITY_TIMEOUT``` - NetAcuity request timeout (ms). By default - ```1000```
* ```LOG_LEVEL``` - log level. Options: "panic", "fatal", "error", "warn", "warning", "info", "debug". By default - ```info```
* ```BROWSCAP_PATH``` - path to browscap DB. Uses only can't dowload last version from browscap.org
* ```GEOIP_CITY_PATH``` - path to GeoIPCity DB. By default - ```/usr/share/GeoIP/GeoIPCity.dat```
* ```GEOIP_NET_SPEED_PATH``` - path to GeoIP2-Connection-Type DB. By default - ```/usr/share/GeoIP/GeoIP2-Connection-Type.mmdb```

## Most useful 'make' commands for develop
* ```make build``` Download dependencies into vendor/ and compile daemon into dist/
* ```make test``` Run tests

See full list - ```make help```
