package api

import (
	"bitbucket.org/kovalevm/cup"
	"github.com/stretchr/testify/suite"
	"google.golang.org/grpc"
	"testing"
)

type ClientProviderTestSuite struct {
	suite.Suite
	container cup.Container
	provider  *ClientProvider
}

func (s *ClientProviderTestSuite) SetupTest() {
	s.container = cup.NewApp()
	s.container.Set("saas.cluster", "test")

	s.provider = new(ClientProvider)
}

func (s *ClientProviderTestSuite) TestRegisterConnection() {
	s.provider.Register(s.container)

	conn, err := s.container.Get("ddsvc.conn")
	s.Require().NoError(err)
	s.Require().IsType((*grpc.ClientConn)(nil), conn)
}

func (s *ClientProviderTestSuite) TestRegisterClient() {
	s.provider.Register(s.container)

	client, err := s.container.Get("ddsvc.client")
	s.Require().NoError(err)
	s.Require().IsType((*deviceDetectorClient)(nil), client)
}

func TestClientProviderTestSuite(t *testing.T) {
	suite.Run(t, new(ClientProviderTestSuite))
}
