package api

type Location struct {
	Country  string `json:"co"`
	Country3 string `json:"co"`
	Region   string `json:"r"`
	City     string `json:"ci"`
}
