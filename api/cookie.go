package api

import "time"

const (
	UuidCookieName         = "sppc_uuid"
	UuidCookieExpirePeriod = time.Second * 31536000
	UuidCookiePath         = "/"
)
