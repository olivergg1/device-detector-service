package api

const (
	BROWSER_ANDROID = "android"
	BROWSER_CHROME  = "chrome"
	BROWSER_FIREFOX = "firefox"
	BROWSER_IE      = "ie"
	BROWSER_EDGE    = "edge"
	BROWSER_NOKIA   = "nokia"
	BROWSER_OPERA   = "opera"
	BROWSER_SAFARI  = "safari"
	BROWSER_UC      = "uc"
	BROWSER_QQ      = "qq"
	BROWSER_OTHERS  = "others"
)

const (
	OS_ANDROID = "android"
	OS_BADA    = "bada"
	OS_CHROME  = "chrome"
	OS_IOS     = "ios"
	OS_LINUX   = "linux"
	OS_MAXOSX  = "macosx"
	OS_NOKIA   = "nokia"
	OS_RIM     = "rim"
	OS_SYMBIAN = "symbian"
	OS_WINDOWS = "win"
	OS_OTHERS  = "others"
)

const (
	DT_PC      = "pc"
	DT_PHONE   = "phone"
	DT_TABLET  = "tablet"
	DT_TV      = "tv"
	DT_CONSOLE = "console"
	DT_OTHERS  = "other"
)

const (
	NetSpeed_UNKNOWN   = 0
	NetSpeed_DIALUP    = 1
	NetSpeed_CABLE_DSL = 2
	NetSpeed_CORPORATE = 3
)
