package api

type Carrier struct {
	MCC     string `xml:"mcc,attr"`
	MNC     string `xml:"mnc,attr"`
	Carrier string `xml:"mobile-carrier,attr"`
}
