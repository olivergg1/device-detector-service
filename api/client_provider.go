package api

import (
	"github.com/sirupsen/logrus"
	"bitbucket.org/kovalevm/cup"
	"fmt"
	"google.golang.org/grpc"
	"google.golang.org/grpc/balancer/roundrobin"
)

type ClientProvider struct{}

func (p *ClientProvider) Register(c cup.Container) {
	p.registerConnection(c)
	p.registerClient(c)
}

func (p *ClientProvider) registerConnection(c cup.Container) {
	c.Set("ddsvc.conn", func(c cup.Container) interface{} {
		conn, err := grpc.Dial(
			p.buildConnectionString(c),
			grpc.WithBalancerName(roundrobin.Name),
			grpc.WithInsecure(),
		)
		if err != nil {
			logger := c.MustGet("logger").(logrus.FieldLogger)
			logger.Fatalf("Couldn't connect to device detector service: %s", err)
		}

		return conn
	})
}

func (p *ClientProvider) registerClient(c cup.Container) {
	c.Set("ddsvc.client", func(c cup.Container) interface{} {
		conn := c.MustGet("ddsvc.conn").(*grpc.ClientConn)

		return NewDeviceDetectorClient(conn)
	})
}

// Build connection string
func (p *ClientProvider) buildConnectionString(c cup.Container) string {
	cluster := c.MustGet("saas.cluster").(string)

	return fmt.Sprintf("registry:///ddsvc-grpc,%s,%s", cluster, CurrentVersion)
}

func (p *ClientProvider) ShutDown(c cup.Container) {
	conn := c.MustGet("ddsvc.conn").(*grpc.ClientConn)
	conn.Close()
}
