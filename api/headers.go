package api

const (
	// ip
	XForwardedForHeaderName = "X-Forwarded-For"
	XClientIPHeaderName     = "X-Client-IP"
	XRealIPHeaderName       = "X-Real-IP"
	XSocketIPHeaderName     = "X-Socket-IP"

	UserAgentHeaderName = "User-Agent"

	AcceptLangHeaderName = "Accept-Language"

	//wurfl
	BrandNameHeaderName          = "Wurfl-Brand-Name"
	ModelNameHeaderName          = "Wurfl-Model-Name"
	DeviceOsHeaderName           = "Wurfl-Device-Os"
	MobileBrowserHeaderName      = "Wurfl-Mobile-Browser"
	IsDesktopHeaderName          = "Wurfl-Is-Desktop"
	IsPhoneHeaderName            = "Wurfl-Is-Phone"
	IsTabletHeaderName           = "Wurfl-Is-Tablet"
	IsSmartTvHeaderName          = "Wurfl-Is-SmartTV"
	IsConsoleHeaderName          = "Wurfl-Is-Console"
	AdvertiserBrowserHeaderName  = "Wurfl-Advertiser-Browser"
	AdvertiserDeviceOsHeaderName = "Wurfl-Advertiser-Device-Os"

	DeviceOsVersionHeaderName           = "Wurfl-Device-Os-Version"
	AdvertiserDeviceOsVersionHeaderName = "Wurfl-Advertiser-Device-Os-Version"
	ResolutionWidthHeaderName           = "Wurfl-Resolution-Width"
	ResolutionHeightHeaderName          = "Wurfl-Resolution-Height"
	IsAppWebviewHeaderName              = "Wurfl-Is-App-Webview"
)

// need for sspsvc
func RequiredHeaders() []string {
	return []string{
		XForwardedForHeaderName,
		XClientIPHeaderName,
		XRealIPHeaderName,
		XSocketIPHeaderName,
		UserAgentHeaderName,
		AcceptLangHeaderName,
		BrandNameHeaderName,
		ModelNameHeaderName,
		DeviceOsHeaderName,
		MobileBrowserHeaderName,
		IsDesktopHeaderName,
		IsPhoneHeaderName,
		IsTabletHeaderName,
		IsSmartTvHeaderName,
		IsConsoleHeaderName,
		AdvertiserBrowserHeaderName,
		AdvertiserDeviceOsHeaderName,
		DeviceOsVersionHeaderName,
		AdvertiserDeviceOsVersionHeaderName,
		ResolutionWidthHeaderName,
		ResolutionHeightHeaderName,
		IsAppWebviewHeaderName,
	}
}
