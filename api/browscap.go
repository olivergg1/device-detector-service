package api

type BrowscapData struct {
	Browser string `json:"br"`
	Os      string `json:"os"`
}
