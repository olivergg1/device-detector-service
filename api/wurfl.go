package api

type WurflData struct {
	DeviceType string `json:"dt"`
	Vendor     string `json:"v"`
	Device     string `json:"d"`
	Browser    string `json:"br"`
	Os         string `json:"os"`

	OsVersion    string `json:"osv"`
	ScreenWidth  uint32 `json:"sw"`
	ScreenHeight uint32 `json:"sh"`
}
