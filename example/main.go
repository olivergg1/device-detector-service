package main

import (
	"bitbucket.org/kovalevm/cup"
	"bitbucket.org/kovalevm/tools/saas"
	"bitbucket.org/kovalevm/tools/sd"
	"github.com/sirupsen/logrus"
	"bitbucket.org/kovalevm/tools/logger"
	"bitbucket.org/kovalevm/tools/grpc"
	"context"
	"bitbucket.org/kovalevm/tools/consul"
	"bitbucket.org/kovalevm/ddsvc/api"
)

func main() {
	app := cup.NewApp()
	app.Register(new(saas.Provider))
	app.Register(new(logger.Provider))
	app.Register(new(consul.Provider))

	app.Register(new(sd.Provider))
	app.Register(new(grpc.ResolverProvider))

	app.Register(new(api.ClientProvider))

	log := app.MustGet("logger").(logrus.FieldLogger)

	log.Infof("Starting client...")

	err := app.Boot()
	if err != nil {
		log.Fatalf("Failed to start client: %s", err)
	}

	client := app.MustGet("ddsvc.client").(api.DeviceDetectorClient)
	ctx := context.Background()

	req := &api.DetectRequest{
		Uuid: "0c781e93-b73b-4315-afe9-ef9a4e27d2e3",
		Headers: map[string]string{
			api.XForwardedForHeaderName:      "172.58.200.207",
			api.XClientIPHeaderName:          "133.54.78.99",
			api.XRealIPHeaderName:            "122.41.66.11",
			api.XSocketIPHeaderName:          "77.88.99.44",
			api.UserAgentHeaderName:          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Safari/537.36",
			api.AcceptLangHeaderName:         "en-us,en;q=0.5",
			api.IsDesktopHeaderName:          "false",
			api.IsPhoneHeaderName:            "true",
			api.IsConsoleHeaderName:          "false",
			api.IsSmartTvHeaderName:          "false",
			api.IsTabletHeaderName:           "false",
			api.AdvertiserBrowserHeaderName:  "Chrome Mobile",
			api.AdvertiserDeviceOsHeaderName: "Linux i686",
			api.BrandNameHeaderName:          "Brand name test",
			api.ModelNameHeaderName:          "Model name test",

			api.AdvertiserDeviceOsVersionHeaderName: "",
			api.DeviceOsVersionHeaderName:           "11.2",
			api.ResolutionHeightHeaderName:          "1920",
			api.ResolutionWidthHeaderName:           "1080",
		},
	}

	res, err := client.Detect(ctx, req)

	if err != nil {
		log.Fatalf("Failed to get visitor info info: %s", err)
	}

	log.Infof("Response visitor info: %+v", res.GetVisitorInfo())

	log.Info("Shutting down the service...")
	app.Shutdown()
	log.Info("Service stopped!")
}
