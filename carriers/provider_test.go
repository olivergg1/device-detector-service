package carriers

import (
	"bitbucket.org/kovalevm/cup"
	"github.com/stretchr/testify/suite"
	"testing"
	"github.com/sirupsen/logrus"
	"github.com/sirupsen/logrus/hooks/test"
	"os"
	"github.com/hashicorp/consul/api"
	"github.com/stretchr/testify/mock"
	"errors"
	"bitbucket.org/kovalevm/tools/mocks"
	ddsvc "bitbucket.org/kovalevm/ddsvc/api"
)

type MockStorageAdapter struct {
	mock.Mock
}

func (a *MockStorageAdapter) Init() error {
	args := a.Called()

	return args.Error(0)
}

func (a *MockStorageAdapter) Shutdown() {
	a.Called()
}

func (a *MockStorageAdapter) Detect(ip string) (*ddsvc.Carrier, error) {
	args := a.Called(ip)

	return args.Get(0).(*ddsvc.Carrier), args.Error(1)
}

type ProviderTestSuite struct {
	suite.Suite
	container  cup.Container
	provider   *Provider
	logger     logrus.FieldLogger
	loggerHook *test.Hook
}

func (s *ProviderTestSuite) SetupTest() {
	s.logger, s.loggerHook = test.NewNullLogger()

	s.container = cup.NewApp()
	s.container.Set("logger", s.logger)

	s.container.Set("consul.client", func(c cup.Container) interface{} {
		client, err := api.NewClient(api.DefaultConfig())
		if err != nil {
			panic(err)
		}

		return client
	})

	s.container.Set("cache", func(c cup.Container) interface{} {
		return new(mocks.Cache)
	})

	s.container.Set("saas.cluster", "test")
	s.provider = new(Provider)
}

func (s *ProviderTestSuite) TestRegisterTimeout() {
	s.provider.Register(s.container)

	timeout, err := s.container.Get("netacuity.timeout")
	s.Require().NoError(err)
	s.Require().Equal(defaultNetAcuityTimeout, timeout)
}

func (s *ProviderTestSuite) TestRegisterCustomTimeout() {
	os.Setenv(netAcuityTimeoutEnvName, "777")
	defer os.Unsetenv(netAcuityTimeoutEnvName)

	s.provider.Register(s.container)

	timeout, err := s.container.Get("netacuity.timeout")

	s.Require().NoError(err)
	s.Require().Equal(777, timeout)
}

func (s *ProviderTestSuite) TestRegisterIncorrectCustomTimeout() {
	incorrectTimeout := "AZINO-777"

	os.Setenv(netAcuityTimeoutEnvName, incorrectTimeout)
	defer os.Unsetenv(netAcuityTimeoutEnvName)

	s.provider.Register(s.container)

	timeout, err := s.container.Get("netacuity.timeout")
	s.Require().NoError(err)
	s.Require().Equal(defaultNetAcuityTimeout, timeout)
}

func (s *ProviderTestSuite) TestRegisterAdapter() {
	s.provider.Register(s.container)

	adapter, err := s.container.Get("carriers.adapter")

	s.Require().NoError(err)
	s.Require().Implements((*StorageAdapter)(nil), adapter)
}

func (s *ProviderTestSuite) TestRegisterStorage() {
	s.provider.Register(s.container)

	storage, err := s.container.Get("carriers.storage")

	s.Require().NoError(err)
	s.Require().Implements((*Storage)(nil), storage)
}

func (s *ProviderTestSuite) TestBoot() {
	adapter := new(MockStorageAdapter)
	adapter.On("Init").Return(nil).Once()

	s.container.Set("carriers.adapter", adapter)

	err := s.provider.Boot(s.container)

	s.Require().NoError(err)
	adapter.AssertExpectations(s.T())
}

func (s *ProviderTestSuite) TestBootError() {
	expErr := errors.New("test error")

	adapter := new(MockStorageAdapter)
	adapter.On("Init").Return(expErr).Once()

	s.container.Set("carriers.adapter", adapter)

	err := s.provider.Boot(s.container)

	s.Require().Equal(expErr, err)
	adapter.AssertExpectations(s.T())
}

func (s *ProviderTestSuite) TestShutdown() {
	adapter := new(MockStorageAdapter)
	adapter.On("Shutdown").Once()

	s.container.Set("carriers.adapter", adapter)

	s.provider.Shutdown(s.container)

	adapter.AssertExpectations(s.T())
}

func TestProviderTestSuite(t *testing.T) {
	suite.Run(t, new(ProviderTestSuite))
}
