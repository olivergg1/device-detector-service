package carriers

import (
	"fmt"
	"bitbucket.org/kovalevm/tools/cache"
	"encoding/xml"
	"github.com/sirupsen/logrus"
	"bitbucket.org/kovalevm/ddsvc/api"
)

type Storage interface {
	Detect(ip string) (*api.Carrier, error)
}

type storage struct {
	adapter StorageAdapter
	cache   cache.Cache
	logger  logrus.FieldLogger
}

func NewStorage(adapter StorageAdapter, cache cache.Cache, logger logrus.FieldLogger) Storage {
	return &storage{
		adapter: adapter,
		cache:   cache,
		logger:  logger,
	}
}

func (s *storage) Detect(ip string) (*api.Carrier, error) {
	logger := s.logger.WithField("ip", ip)

	carrier := &api.Carrier{}

	cacheKey := s.getCacheKey(ip)
	data, err := s.cache.Get(cacheKey)
	if err == nil {
		err = xml.Unmarshal(data, carrier)
		if err == nil {
			return carrier, nil
		} else {
			logger.Errorf("Failed to parse stored in cache data - '%v'", data)
		}
	}

	carrier, err = s.adapter.Detect(ip)
	if err != nil {
		return nil, err
	}

	cacheData, err := xml.Marshal(carrier)
	if err == nil {
		err = s.cache.Set(cacheKey, cacheData)
		if err != nil {
			logger.Errorf("Failed to cache carrier - '%v'", *carrier)
		}
	} else {
		logger.Errorf("Failed to marshal carrier - '%v'", *carrier)
	}

	return carrier, err
}

func (s *storage) getCacheKey(ip string) string {
	return fmt.Sprintf("netacuity_%v", ip)
}
