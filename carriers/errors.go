package carriers

import "errors"

var (
	NoAvailableNodesErr = errors.New("no available NetAcuity nodes")
)
