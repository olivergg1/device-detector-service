package carriers

import (
	"github.com/stretchr/testify/suite"
	"github.com/sirupsen/logrus"
	"github.com/hashicorp/consul/api"
	"github.com/sirupsen/logrus/hooks/test"
	"testing"
	ddApi "bitbucket.org/kovalevm/ddsvc/api"
)

type NetAcuityAdapterTestSuite struct {
	suite.Suite
	consul  *api.Client
	logger  logrus.FieldLogger
	adapter StorageAdapter
}

func (s *NetAcuityAdapterTestSuite) SetupTest() {
	var err error

	s.consul, err = api.NewClient(api.DefaultConfig())
	if err != nil {
		s.T().Fatalf("Failed to create consul client: %s", err)
	}
	s.logger, _ = test.NewNullLogger()

	s.adapter = NewNetAcuityAdapter(s.consul, s.logger, "test", 1000)

	err = s.adapter.Init()
	if err != nil {
		s.T().Fatalf("Failed to initialize adapter: %s", err)
	}
}

func (s *NetAcuityAdapterTestSuite) TestDetect() {
	expCarrier := &ddApi.Carrier{MCC: "0", MNC: "0", Carrier:""}

	carrier, err := s.adapter.Detect("0.0.0.0")

	s.Require().NoError(err)
	s.Require().Equal(expCarrier, carrier)
}

func (s *NetAcuityAdapterTestSuite) TestDetectNoAvailableNodes() {
	s.adapter = NewNetAcuityAdapter(s.consul, s.logger, "test_test", 1000)

	err := s.adapter.Init()
	if err != nil {
		s.T().Fatalf("Failed to initialize adapter: %s", err)
	}

	_, err = s.adapter.Detect("0.0.0.0")

	s.Require().Equal(NoAvailableNodesErr, err)
}

func (s *NetAcuityAdapterTestSuite) TestDetectInvalidIP() {
	_, err := s.adapter.Detect("invalid-ip")

	s.Require().NotNil(err)
}

func TestConsulServiceAdapterTestSuite(t *testing.T) {
	suite.Run(t, new(NetAcuityAdapterTestSuite))
}
