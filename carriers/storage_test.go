package carriers

import (
	"github.com/stretchr/testify/suite"
	"bitbucket.org/kovalevm/tools/mocks"
	"errors"
	"github.com/sirupsen/logrus/hooks/test"
	"github.com/sirupsen/logrus"
	"fmt"
	"encoding/xml"
	"github.com/stretchr/testify/mock"
	"testing"
	"bitbucket.org/kovalevm/ddsvc/api"
)

type StorageTestSuite struct {
	suite.Suite
	adapter    *MockStorageAdapter
	cache      *mocks.Cache
	logger     logrus.FieldLogger
	loggerHook *test.Hook
	storage    Storage
}

func (s *StorageTestSuite) SetupTest() {
	s.adapter = new(MockStorageAdapter)
	s.cache = new(mocks.Cache)
	s.logger, s.loggerHook = test.NewNullLogger()

	s.storage = NewStorage(s.adapter, s.cache, s.logger)
}

func (s *StorageTestSuite) TestDetect() {
	ip := "123.123.123.123"
	cacheKey := fmt.Sprintf("netacuity_%v", ip)
	expCarrier := &api.Carrier{MCC:"01", MNC:"20", Carrier:"t-mobile"}

	s.cache.On("Get", cacheKey).Return(([]byte)(nil), errors.New("not found")).Once()

	s.adapter.On("Detect", ip).Return(expCarrier, nil).Once()

	data, _ := xml.Marshal(expCarrier)
	matcher := mock.MatchedBy(func(arg []byte) bool {
		return s.Assert().Equal(data, arg)
	})
	s.cache.On("Set", cacheKey, matcher).Return(nil).Once()

	carrier, err := s.storage.Detect(ip)
	s.Require().NoError(err)
	s.Require().Equal(expCarrier, carrier)
	s.cache.AssertExpectations(s.T())
	s.adapter.AssertExpectations(s.T())
}

func (s *StorageTestSuite) TestDetectFromCache() {
	ip := "123.123.123.123"
	cacheKey := fmt.Sprintf("netacuity_%v", ip)
	expCarrier := &api.Carrier{MCC:"01", MNC:"20", Carrier:"t-mobile"}

	data, _ := xml.Marshal(expCarrier)
	s.cache.On("Get", cacheKey).Return(data, nil).Once()

	carrier, err := s.storage.Detect(ip)
	s.Require().NoError(err)
	s.Require().Equal(expCarrier, carrier)

	s.cache.AssertExpectations(s.T())
	s.cache.AssertNumberOfCalls(s.T(), "Set", 0)
	s.adapter.AssertNumberOfCalls(s.T(), "Detect", 0)
}

func (s *StorageTestSuite) TestDetectAdapterError() {
	ip := "123.123.123.123"
	cacheKey := fmt.Sprintf("netacuity_%v", ip)

	s.cache.On("Get", cacheKey).Return(([]byte)(nil), errors.New("not found")).Once()

	expErr := errors.New("adapter error")
	s.adapter.On("Detect", ip).Return((*api.Carrier)(nil), expErr).Once()

	_, err := s.storage.Detect(ip)
	s.Require().Error(err)
	s.Require().Equal(expErr, err)

	s.cache.AssertExpectations(s.T())
	s.adapter.AssertExpectations(s.T())
	s.cache.AssertNumberOfCalls(s.T(), "Set", 0)
}

func TestServerTestSuite(t *testing.T) {
	suite.Run(t, new(StorageTestSuite))
}
