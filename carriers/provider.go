package carriers

import (
	"bitbucket.org/kovalevm/cup"
	"github.com/sirupsen/logrus"
	"github.com/hashicorp/consul/api"
	"os"
	"strconv"
	"bitbucket.org/kovalevm/tools/cache"
)

const netAcuityTimeoutEnvName = "NET_ACUITY_TIMEOUT"
const defaultNetAcuityTimeout = 1000 // in ms

type Provider struct{}

func (p *Provider) Register(c cup.Container) {
	p.registerNetAcuityTimeout(c)

	p.registerAdapter(c)

	p.registerStorage(c)
}

func (p *Provider) Boot(c cup.Container) error {
	a := c.MustGet("carriers.adapter").(StorageAdapter)

	return a.Init()
}

// Shutdown config service adapter on app stop
func (p *Provider) Shutdown(c cup.Container) {
	a := c.MustGet("carriers.adapter").(StorageAdapter)
	a.Shutdown()
}

func (p *Provider) registerAdapter(c cup.Container) {
	c.Set("carriers.adapter", func(c cup.Container) interface{} {
		client := c.MustGet("consul.client").(*api.Client)
		cluster := c.MustGet("saas.cluster").(string)
		timeout := c.MustGet("netacuity.timeout").(int)

		logger := c.MustGet("logger").(logrus.FieldLogger)
		logger = logger.WithField("service", "carriers")

		return NewNetAcuityAdapter(client, logger, cluster, timeout)
	})
}

func (p *Provider) registerStorage(c cup.Container) {
	c.Set("carriers.storage", func(c cup.Container) interface{} {
		a := c.MustGet("carriers.adapter").(StorageAdapter)
		appCache := c.MustGet("cache").(cache.Cache)

		logger := c.MustGet("logger").(logrus.FieldLogger)
		logger = logger.WithField("service", "carriers")

		return NewStorage(a, appCache, logger)
	})
}

func (p *Provider) registerNetAcuityTimeout(c cup.Container) {
	c.Set("netacuity.timeout", func(c cup.Container) interface{} {
		env, exist := os.LookupEnv(netAcuityTimeoutEnvName)

		if exist && env != "" {
			timeout, err := strconv.Atoi(env)
			if err != nil {

				logger := c.MustGet("logger").(logrus.FieldLogger)
				logger = logger.WithField("service", "carriers")

				logger.Errorf(
					"Failed parse '%v' env variable. Value - %v. Error - %v",
					netAcuityTimeoutEnvName,
					env,
					err,
				)
			} else {
				return timeout
			}
		}

		return defaultNetAcuityTimeout
	})
}
