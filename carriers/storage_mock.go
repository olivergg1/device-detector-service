package carriers

import (
	"github.com/stretchr/testify/mock"
	"bitbucket.org/kovalevm/ddsvc/api"
)

type MockStorage struct {
	mock.Mock
}

func (a *MockStorage) Detect(ip string) (*api.Carrier, error) {
	args := a.Called(ip)

	return args.Get(0).(*api.Carrier), args.Error(1)
}
