package carriers

import "bitbucket.org/kovalevm/ddsvc/api"

type StorageAdapter interface {
	Init() error

	Shutdown()

	Detect(ip string) (*api.Carrier, error)
}
