package carriers

import (
	"bitbucket.org/kovalevm/netacuity"
	"time"
	"math/rand"
	"github.com/sirupsen/logrus"
	"sync"
	"context"
	"github.com/hashicorp/consul/api"
	"encoding/xml"
	ddApi "bitbucket.org/kovalevm/ddsvc/api"
	"strings"
)

type NetAcuityAdapter struct {
	nodes    []string // netacuity nodes
	consul   *api.Client
	logger   logrus.FieldLogger
	mu       sync.RWMutex
	ctx      context.Context
	cancel   context.CancelFunc
	wg       sync.WaitGroup
	lastIdx  uint64
	watchers []chan struct{}
	cluster  string // TODO saas.cluster
	timeout  int    // netacuity request timeout
}

func NewNetAcuityAdapter(consul *api.Client, logger logrus.FieldLogger, cluster string, timeout int) StorageAdapter {
	ctx, cancel := context.WithCancel(context.Background())

	return &NetAcuityAdapter{
		consul:  consul,
		logger:  logger.WithField("service", "carriers"),
		ctx:     ctx,
		cancel:  cancel,
		cluster: cluster,
		timeout: timeout,
	}
}

func (adapter *NetAcuityAdapter) Init() error {
	err := adapter.loadNodes()
	if err != nil {
		adapter.logger.Errorf("Failed to initialize consul: %s", err)
		return err
	}

	adapter.wg.Add(1)
	go adapter.watcher()

	return nil
}

func (adapter *NetAcuityAdapter) Shutdown() {
	adapter.cancel()
	adapter.wg.Wait()
}

func (adapter *NetAcuityAdapter) Detect(ip string) (*ddApi.Carrier, error) {
	logger := adapter.logger.WithField("ip", ip)

	if len(adapter.nodes) == 0 {
		return nil, NoAvailableNodesErr
	}

	random := rand.New(rand.NewSource(time.Now().Unix()))
	node := adapter.nodes[random.Intn(len(adapter.getNodes()))]

	logger.Debugf("NetAcuity node: %v", node)

	var result string
	result, err := netacuity.QueryXml([5]interface{}{"24", 7, ip, node, adapter.timeout})
	if err != nil {
		return nil, err
	}

	logger.Debugf("NetAcuity raw result: %v", result)

	var carrier ddApi.Carrier
	err = xml.Unmarshal([]byte(result), &carrier)
	if err != nil {
		return nil, err
	}

	if carrier.Carrier == "?" {
		carrier.Carrier = ""
	}

	return &carrier, nil
}

func (adapter *NetAcuityAdapter) loadNodes() error {
	opts := &api.QueryOptions{WaitIndex: adapter.lastIdx}
	opts = opts.WithContext(adapter.ctx)

	entries, meta, err := adapter.consul.Health().Service("netacuity", adapter.cluster, true, opts)
	if err != nil {
		return err
	}

	adapter.lastIdx = meta.LastIndex

	var nodes [] string
	for _, entry := range entries {
		host := entry.Node.Address
		if entry.Service.Address != "" {
			host = entry.Service.Address
		}

		nodes = append(nodes, host)
	}

	adapter.mu.Lock()
	defer adapter.mu.Unlock()

	adapter.nodes = nodes

	return nil
}

func (adapter *NetAcuityAdapter) watcher() {
	defer adapter.wg.Done()
	for {
		select {
		case <-adapter.ctx.Done():
			return
		default:
			err := adapter.loadNodes()
			if err != nil && !strings.HasSuffix(err.Error(), context.Canceled.Error()) {
				adapter.logger.Errorf("Failed to load netacuity nodes data: %s", err)

				time.Sleep(1000 * time.Millisecond)
			}
		}
	}
}

func (adapter *NetAcuityAdapter) getNodes() []string {
	adapter.mu.Lock()
	defer adapter.mu.Unlock()

	return adapter.nodes
}
