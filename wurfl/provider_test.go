package wurfl

import (
	"bitbucket.org/kovalevm/cup"
	"github.com/stretchr/testify/suite"
	"testing"
	"github.com/sirupsen/logrus"
	"github.com/sirupsen/logrus/hooks/test"
)

type ProviderTestSuite struct {
	suite.Suite
	container  cup.Container
	provider   *Provider
	logger     logrus.FieldLogger
	loggerHook *test.Hook
}

func (s *ProviderTestSuite) SetupTest() {
	s.logger, s.loggerHook = test.NewNullLogger()

	s.container = cup.NewApp()
	s.container.Set("logger", s.logger)

	s.provider = new(Provider)
}

func (s *ProviderTestSuite) TestRegisterStorage() {
	s.provider.Register(s.container)

	storage, err := s.container.Get("wurfl.storage")

	s.Require().NoError(err)
	s.Require().Implements((*Storage)(nil), storage)
}

func TestProviderTestSuite(t *testing.T) {
	suite.Run(t, new(ProviderTestSuite))
}
