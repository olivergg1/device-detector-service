package wurfl

import (
	"bitbucket.org/kovalevm/cup"
	"github.com/sirupsen/logrus"
)

type Provider struct{}

func (p *Provider) Register(c cup.Container) {
	c.Set("wurfl.storage", func(c cup.Container) interface{} {
		logger := c.MustGet("logger").(logrus.FieldLogger)

		return NewStorage(logger)
	})
}
