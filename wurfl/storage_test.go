package wurfl

import (
	"github.com/stretchr/testify/suite"
	"github.com/sirupsen/logrus"
	"github.com/sirupsen/logrus/hooks/test"
	"testing"
	"bitbucket.org/kovalevm/ddsvc/api"
)

type StorageTestSuite struct {
	suite.Suite
	logger     logrus.FieldLogger
	loggerHook *test.Hook
	storage    Storage
}

func (s *StorageTestSuite) SetupTest() {
	s.logger, s.loggerHook = test.NewNullLogger()
	s.storage = NewStorage(s.logger)
}

func (s *StorageTestSuite) TestParsePc() {
	headers := map[string]string{
		api.IsDesktopHeaderName:          "true",
		api.IsPhoneHeaderName:            "false",
		api.IsConsoleHeaderName:          "false",
		api.IsSmartTvHeaderName:          "false",
		api.IsTabletHeaderName:           "false",
		api.AdvertiserBrowserHeaderName:  "Opera Link Sync",
		api.AdvertiserDeviceOsHeaderName: "Linux i686",
		api.BrandNameHeaderName:          "Brand name test",
		api.ModelNameHeaderName:          "Model name test",

		api.AdvertiserDeviceOsVersionHeaderName: "",
		api.DeviceOsVersionHeaderName:           "11.2",
		api.ResolutionHeightHeaderName:          "1920",
		api.ResolutionWidthHeaderName:           "1080",
	}

	expected := &api.WurflData{
		DeviceType: api.DT_PC,
		Browser:    api.BROWSER_OPERA,
		Os:         api.OS_LINUX,
		Vendor:     "",
		Device:     "Model name test",

		OsVersion:    "11.2",
		ScreenWidth:  1080,
		ScreenHeight: 1920,
	}

	actual := s.storage.Parse(headers)
	s.Require().Equal(expected, actual)
}

func (s *StorageTestSuite) TestParsePhone() {
	headers := map[string]string{
		api.IsDesktopHeaderName:          "false",
		api.IsPhoneHeaderName:            "true",
		api.IsConsoleHeaderName:          "false",
		api.IsSmartTvHeaderName:          "false",
		api.IsTabletHeaderName:           "false",
		api.AdvertiserBrowserHeaderName:  "Chrome Mobile",
		api.AdvertiserDeviceOsHeaderName: "iOS",
		api.BrandNameHeaderName:          "Brand name test",
		api.ModelNameHeaderName:          "Model name test",

		api.AdvertiserDeviceOsVersionHeaderName: "",
		api.DeviceOsVersionHeaderName:           "11.2",
		api.ResolutionHeightHeaderName:          "1920",
		api.ResolutionWidthHeaderName:           "1080",
	}

	expected := &api.WurflData{
		DeviceType: api.DT_PHONE,
		Browser:    api.BROWSER_CHROME,
		Os:         api.OS_IOS,
		Vendor:     "Brand name test",
		Device:     "Model name test",

		OsVersion:    "11.2",
		ScreenWidth:  1080,
		ScreenHeight: 1920,
	}

	actual := s.storage.Parse(headers)
	s.Require().Equal(expected, actual)
}

func (s *StorageTestSuite) TestParseTablet() {
	headers := map[string]string{
		api.IsDesktopHeaderName:          "false",
		api.IsPhoneHeaderName:            "false",
		api.IsConsoleHeaderName:          "false",
		api.IsSmartTvHeaderName:          "false",
		api.IsTabletHeaderName:           "true",
		api.AdvertiserBrowserHeaderName:  "Opera Tablet",
		api.AdvertiserDeviceOsHeaderName: "Android",
		api.BrandNameHeaderName:          "Brand name test",
		api.ModelNameHeaderName:          "Model name test",

		api.AdvertiserDeviceOsVersionHeaderName: "99.3",
		api.DeviceOsVersionHeaderName:           "11.2",
		api.ResolutionHeightHeaderName:          "1920",
		api.ResolutionWidthHeaderName:           "1080",
	}

	expected := &api.WurflData{
		DeviceType: api.DT_TABLET,
		Browser:    api.BROWSER_OPERA,
		Os:         api.OS_ANDROID,
		Vendor:     "Brand name test",
		Device:     "Model name test",

		OsVersion:    "99.3",
		ScreenWidth:  1080,
		ScreenHeight: 1920,
	}

	actual := s.storage.Parse(headers)
	s.Require().Equal(expected, actual)
}

func (s *StorageTestSuite) TestParseEmptyHeaders() {
	headers := map[string]string{}

	expected := &api.WurflData{
		DeviceType: api.DT_OTHERS,
	}

	actual := s.storage.Parse(headers)
	s.Require().Equal(expected, actual)
	s.Require().Equal(logrus.ErrorLevel, s.loggerHook.LastEntry().Level)
}

func TestServerTestSuite(t *testing.T) {
	suite.Run(t, new(StorageTestSuite))
}
