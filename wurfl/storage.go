package wurfl

import (
	"github.com/sirupsen/logrus"
	"bitbucket.org/kovalevm/ddsvc/api"
	"strconv"
)

type Storage interface {
	Parse(headers map[string]string) *api.WurflData
}

type storage struct {
	logger logrus.FieldLogger
}

func NewStorage(logger logrus.FieldLogger) Storage {
	return &storage{
		logger: logger.WithField("service", "wurfl"),
	}
}

func (s *storage) header(headers map[string]string, headerName string) string {
	value, ok := headers[headerName]
	if !ok {
		s.logger.Errorf("failed to find '%s' in headers", headerName)
	}

	return value
}

func (s *storage) Parse(headers map[string]string) (*api.WurflData) {
	data := &api.WurflData{}

	// detect device type
	if s.header(headers, api.IsDesktopHeaderName) == "true" {
		data.DeviceType = api.DT_PC
	} else if s.header(headers, api.IsPhoneHeaderName) == "true" {
		data.DeviceType = api.DT_PHONE
	} else if s.header(headers, api.IsTabletHeaderName) == "true" {
		data.DeviceType = api.DT_TABLET
	} else if s.header(headers, api.IsConsoleHeaderName) == "true" {
		data.DeviceType = api.DT_CONSOLE
	} else if s.header(headers, api.IsSmartTvHeaderName) == "true" {
		data.DeviceType = api.DT_TV
	} else {
		data.DeviceType = api.DT_OTHERS
	}

	// detect browser
	if wurflBrowser := s.header(headers, api.AdvertiserBrowserHeaderName); wurflBrowser != "" {
		if browser, ok := browsersMap[wurflBrowser]; ok {
			data.Browser = browser
		} else {
			s.logger.Infof("Unknown WURFL browser: %s", wurflBrowser)
		}
	}

	// detect os
	if wurflOs := s.header(headers, api.AdvertiserDeviceOsHeaderName); wurflOs != "" {
		if os, ok := osMap[wurflOs]; ok {
			data.Os = os
		} else {
			s.logger.Warnf("Unknown WURFL os: %s", wurflOs)
		}
	}

	// detect vendor
	if data.DeviceType != api.DT_PC {
		data.Vendor = s.header(headers, api.BrandNameHeaderName)
	}

	// detect device model
	data.Device = s.header(headers, api.ModelNameHeaderName)

	// detect os version
	data.OsVersion = s.header(headers, api.AdvertiserDeviceOsVersionHeaderName)
	if data.OsVersion == "" {
		data.OsVersion = s.header(headers, api.DeviceOsVersionHeaderName)
	}

	// detect screen width
	str := s.header(headers, api.ResolutionWidthHeaderName)
	if integer, err := strconv.Atoi(str); err == nil {
		data.ScreenWidth = uint32(integer)
	}

	// detect screen height
	str = s.header(headers, api.ResolutionHeightHeaderName)
	if integer, err := strconv.Atoi(str); err == nil {
		data.ScreenHeight = uint32(integer)
	}

	return data
}
