package wurfl

import (
	"github.com/stretchr/testify/mock"
	"bitbucket.org/kovalevm/ddsvc/api"
)

type MockStorage struct {
	mock.Mock
}

func (s *MockStorage) Parse(headers map[string]string) *api.WurflData {
	args := s.Called(headers)

	return args.Get(0).(*api.WurflData)
}
