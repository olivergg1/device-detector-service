package server

import (
	"github.com/stretchr/testify/suite"
	"testing"
)

type UtilsTestSuite struct {
	suite.Suite
}

func (s *UtilsTestSuite) TestDetectLangErr() {
	_, err := detectLang("s")
	s.Require().Error(err)
}

func (s *UtilsTestSuite) TestDetectLang() {
	lang, err := detectLang("ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7")
	s.Require().Equal("RU", lang)
	s.Require().NoError(err)
}

func (s *UtilsTestSuite) TestDetectLangUnknown() {
	lang, err := detectLang("pp-PP,ru;q=0.9,en-US;q=0.8,en;q=0.7")
	s.Require().Equal("UN", lang)
	s.Require().NoError(err)
}

func (s *UtilsTestSuite) TestExtractIpFromForwardedFor() {
	var ff, ip string
	var err error

	ff = "123.123.123.123"
	ip, err = parseValidIp(ff)
	s.Require().NoError(err)
	s.Require().Equal("123.123.123.123", ip)

	ff = " 123.123.123.123 "
	ip, err = parseValidIp(ff)
	s.Require().NoError(err)
	s.Require().Equal("123.123.123.123", ip)

	ff = "www.sua.ru "
	ip, err = parseValidIp(ff)
	s.Require().Error(err)

	ff = "88.88.88.123, 1.1.1.1"
	ip, err = parseValidIp(ff)
	s.Require().NoError(err)
	s.Require().Equal("88.88.88.123", ip)

	ff = " 88.88.88.123,  1.1.1.1,  2.2.2.2"
	ip, err = parseValidIp(ff)
	s.Require().NoError(err)
	s.Require().Equal("88.88.88.123", ip)

	ff = ""
	ip, err = parseValidIp(ff)
	s.Require().Error(err)

	ff = " "
	ip, err = parseValidIp(ff)
	s.Require().Error(err)

	ff = "fdsafsafaf"
	ip, err = parseValidIp(ff)
	s.Require().Error(err)

	ff = "fdsafsafaf, 5.5.5.5"
	ip, err = parseValidIp(ff)
	s.Require().NoError(err)
	s.Require().Equal("5.5.5.5", ip)

	ff = "-, unknown"
	ip, err = parseValidIp(ff)
	s.Require().Error(err)

	ff = "777.777.777.777"
	ip, err = parseValidIp(ff)
	s.Require().Error(err)

	ff = "FE80:0000:0000:0000:0202:B3FF:FE1E:8329"
	ip, err = parseValidIp(ff)
	s.Require().Error(err)
}

func TestUtilsTestSuite(t *testing.T) {
	suite.Run(t, new(UtilsTestSuite))
}
