package server

import (
	"bitbucket.org/kovalevm/ddsvc/api"
	"strings"
	"fmt"
	"net"
)

func detectIp(headers map[string]string) (string, error) {
	var (
		ip  string
		err error
	)

	if ip, err = parseValidIp(headers[api.XForwardedForHeaderName]); err != nil {
		if ip, err = parseValidIp(headers[api.XRealIPHeaderName]); err != nil {
			if ip, err = parseValidIp(headers[api.XClientIPHeaderName]); err != nil {
				if ip, err = parseValidIp(headers[api.XSocketIPHeaderName]); err != nil {
					return "", fmt.Errorf(
						"all of IP headers don't contains valid IP: %s - '%s', %s - '%s', %s - '%s', %s - '%s'",
						api.XForwardedForHeaderName, headers[api.XForwardedForHeaderName],
						api.XRealIPHeaderName, headers[api.XRealIPHeaderName],
						api.XClientIPHeaderName, headers[api.XClientIPHeaderName],
						api.XSocketIPHeaderName, headers[api.XSocketIPHeaderName],
					)
				}
			}
		}
	}

	return ip, nil
}

func parseValidIp(str string) (string, error) {
	arr := strings.Split(str, ",")

	for _, item := range arr {
		item = strings.TrimSpace(item)
		ip := net.ParseIP(item)
		if ip != nil {
			ip = ip.To4()
			// only ipv4 format
			if ip != nil {
				return ip.String(), nil
			}
		}
	}

	return "", fmt.Errorf("incorrect IP value: %s", str)
}

var languages = []string{
	"AA", "AB", "AE", "AF", "AK", "AM", "AN", "AR", "AS", "AV", "AY", "AZ", "BA", "BE", "BG",
	"BH", "BI", "BM", "BN", "BO", "BR", "BS", "CA", "CE", "CH", "CO", "CR", "CS", "CU", "CV",
	"CY", "DA", "DE", "DV", "DZ", "EE", "EL", "EN", "EO", "ES", "ET", "EU", "FA", "FF", "FI",
	"FJ", "FO", "FR", "FY", "GA", "GD", "GL", "GN", "GU", "GV", "HA", "HE", "HI", "HO", "HR",
	"HT", "HU", "HY", "HZ", "IA", "ID", "IE", "IG", "II", "IK", "IO", "IS", "IT", "IU", "JA",
	"JV", "KA", "KG", "KI", "KJ", "KK", "KL", "KM", "KN", "KO", "KR", "KS", "KU", "KV", "KW",
	"KY", "LA", "LB", "LG", "LI", "LN", "LO", "LT", "LU", "LV", "MG", "MH", "MI", "MK", "ML",
	"MN", "MR", "MS", "MT", "MY", "NA", "NB", "ND", "NE", "NG", "NL", "NN", "NO", "NR", "NV",
	"NY", "OC", "OJ", "OM", "OR", "OS", "PA", "PI", "PL", "PS", "PT", "QU", "RM", "RN", "RO",
	"RU", "RW", "SA", "SC", "SD", "SE", "SG", "SH", "SI", "SK", "SL", "SM", "SN", "SO", "SQ",
	"SR", "SS", "ST", "SU", "SV", "SW", "TA", "TE", "TG", "TH", "TI", "TK", "TL", "TN", "TO",
	"TR", "TS", "TT", "TW", "TY", "UG", "UK", "UN", "UR", "UZ", "VE", "VI", "VO", "WA", "WO",
	"XH", "YI", "YO", "ZA", "ZH", "ZU",
}

func detectLang(acceptLanguage string) (string, error) {
	unknownLang := "UN"
	if len(acceptLanguage) < 2 {
		return unknownLang, fmt.Errorf("AcceptLanguage must be at least then 2 chars, given: '%s'", acceptLanguage)
	}

	language := strings.ToUpper(acceptLanguage[0:2])
	for _, item := range languages {
		if item == language {
			return language, nil
		}
	}

	return unknownLang, nil
}
