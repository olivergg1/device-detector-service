package server

import (
	"bitbucket.org/kovalevm/cup"
	"github.com/sirupsen/logrus"
	"bitbucket.org/kovalevm/ddsvc/carriers"
	"bitbucket.org/kovalevm/ddsvc/browscap"
	"google.golang.org/grpc"
	"bitbucket.org/kovalevm/ddsvc/location"
	"bitbucket.org/kovalevm/ddsvc/api"
	"bitbucket.org/kovalevm/ddsvc/net_speed"
	"bitbucket.org/kovalevm/ddsvc/wurfl"
)

type Provider struct{}

func (p *Provider) Register(c cup.Container) {
	p.registerServer(c)
}

func (p *Provider) registerServer(c cup.Container) {
	c.Set("ddsvc.server", func(c cup.Container) interface{} {
		carriersStorage := c.MustGet("carriers.storage").(carriers.Storage)
		browscapStorage := c.MustGet("browscap.storage").(browscap.Storage)
		locationStorage := c.MustGet("location.storage").(location.Storage)
		netSpeedStorage := c.MustGet("net_speed.storage").(net_speed.Storage)
		wurflStorage := c.MustGet("wurfl.storage").(wurfl.Storage)
		logger := c.MustGet("logger").(logrus.FieldLogger)

		return NewDeviceDetectorServer(
			carriersStorage,
			browscapStorage,
			locationStorage,
			netSpeedStorage,
			wurflStorage,
			logger,
		)
	})

	c.MustExtend("grpc.server", func(old interface{}, c cup.Container) interface{} {
		grpcServer := old.(*grpc.Server)
		ddServer := c.MustGet("ddsvc.server").(api.DeviceDetectorServer)

		api.RegisterDeviceDetectorServer(grpcServer, ddServer)

		return grpcServer
	})
}
