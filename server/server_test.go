package server

import (
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"bitbucket.org/kovalevm/ddsvc/carriers"
	"bitbucket.org/kovalevm/ddsvc/browscap"
	"bitbucket.org/kovalevm/ddsvc/net_speed"
	"bitbucket.org/kovalevm/ddsvc/location"
	"github.com/sirupsen/logrus/hooks/test"
	"bitbucket.org/kovalevm/ddsvc/api"
	"context"
	"testing"
	"bitbucket.org/kovalevm/ddsvc/wurfl"
	"errors"
)

const (
	testIp          = "8.8.8.8"
	testUuid        = "0c781e93-b73b-4315-afe9-ef9a4e27d2e3"
	testUa          = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Safari/537.36"
	testAccLanguage = "en-us,en;q=0.5"
	testLanguage    = "EN"
)

type ServerTestSuite struct {
	suite.Suite
	carriers   *carriers.MockStorage
	browscap   *browscap.MockStorage
	location   *location.MockStorage
	netSpeed   *net_speed.MockStorage
	wurfl      *wurfl.MockStorage
	logger     logrus.FieldLogger
	loggerHook *test.Hook
	server     api.DeviceDetectorServer
	ctx        context.Context
}

func (s *ServerTestSuite) SetupTest() {
	s.carriers = new(carriers.MockStorage)
	s.browscap = new(browscap.MockStorage)
	s.location = new(location.MockStorage)
	s.netSpeed = new(net_speed.MockStorage)
	s.wurfl = new(wurfl.MockStorage)
	s.logger, s.loggerHook = test.NewNullLogger()
	s.server = NewDeviceDetectorServer(s.carriers, s.browscap, s.location, s.netSpeed, s.wurfl, s.logger)
}

func (s *ServerTestSuite) TestDetectMobile() {
	testVendor := "Apple"
	testDevice := "iPhone X"
	testLoc := &api.Location{Country: "US", Region: "CA", City: "new york", Country3: "USA"}
	testCarrier := &api.Carrier{MCC: "147", MNC: "099", Carrier: "t-mobile"}
	testNetSpeed := uint32(api.NetSpeed_CORPORATE)

	req := &api.DetectRequest{
		Uuid: testUuid,
		Headers: map[string]string{
			api.XForwardedForHeaderName:      testIp,
			api.XClientIPHeaderName:          "133.54.78.99",
			api.XRealIPHeaderName:            "122.41.66.11",
			api.XSocketIPHeaderName:          "77.88.99.44",
			api.UserAgentHeaderName:          testUa,
			api.AcceptLangHeaderName:         testAccLanguage,
			api.IsDesktopHeaderName:          "false",
			api.IsPhoneHeaderName:            "true",
			api.IsConsoleHeaderName:          "false",
			api.IsSmartTvHeaderName:          "false",
			api.IsTabletHeaderName:           "false",
			api.AdvertiserBrowserHeaderName:  "Safari",
			api.AdvertiserDeviceOsHeaderName: "iOS",
			api.BrandNameHeaderName:          testVendor,
			api.ModelNameHeaderName:          testDevice,

			api.AdvertiserDeviceOsVersionHeaderName: "",
			api.DeviceOsVersionHeaderName:           "11.2",
			api.ResolutionHeightHeaderName:          "1920",
			api.ResolutionWidthHeaderName:           "1080",
		},
	}

	wurflData := &api.WurflData{
		DeviceType: api.DT_PHONE,
		Browser:    api.BROWSER_SAFARI,
		Os:         api.OS_IOS,
		Vendor:     testVendor,
		Device:     testDevice,

		OsVersion:    "11.2",
		ScreenWidth:  1080,
		ScreenHeight: 1920,
	}

	expInfo := &api.VisitorInfo{
		Uuid:           testUuid,
		AcceptLanguage: testAccLanguage,
		Language:       testLanguage,
		UserAgent:      testUa,
		Ip:             testIp,
		Country:        testLoc.Country,
		Country3:       testLoc.Country3,
		Region:         testLoc.Region,
		City:           testLoc.City,
		DeviceType:     api.DT_PHONE,
		Browser:        api.BROWSER_SAFARI,
		Os:             api.OS_IOS,
		Mcc:            testCarrier.MCC,
		Mnc:            testCarrier.MNC,
		Carrier:        testCarrier.Carrier,
		NetSpeed:       testNetSpeed,
		Vendor:         testVendor,
		Device:         testDevice,
		OsVersion:      "11.2",
		ScreenWidth:    1080,
		ScreenHeight:   1920,
	}

	s.location.On("GetLocation", testIp).Return(testLoc).Once()
	defer s.location.AssertExpectations(s.T())

	s.wurfl.On("Parse", req.Headers).Return(wurflData).Once()
	defer s.wurfl.AssertExpectations(s.T())

	s.carriers.On("Detect", testIp).Return(testCarrier, nil).Once()
	defer s.carriers.AssertExpectations(s.T())

	s.netSpeed.On("GetNetSpeed", testIp).Return(testNetSpeed, nil).Once()
	defer s.netSpeed.AssertExpectations(s.T())

	res, err := s.server.Detect(context.Background(), req)

	s.Require().NoError(err)
	s.Require().Equal(expInfo, res.VisitorInfo)

	s.browscap.AssertNumberOfCalls(s.T(), "GetBrowser", 0)
	s.browscap.AssertNumberOfCalls(s.T(), "GetOs", 0)
}

func (s *ServerTestSuite) TestDetectPc() {
	testLoc := &api.Location{Country: "RU", Region: "RO", City: "rostov", Country3: "RUS"}
	testNetSpeed := uint32(api.NetSpeed_CABLE_DSL)

	req := &api.DetectRequest{
		Uuid: testUuid,
		Headers: map[string]string{
			api.XClientIPHeaderName:          "122.41.66.11",
			api.XRealIPHeaderName:            testIp,
			api.XSocketIPHeaderName:          "77.88.99.44",
			api.UserAgentHeaderName:          testUa,
			api.AcceptLangHeaderName:         testAccLanguage,
			api.IsDesktopHeaderName:          "true",
			api.IsPhoneHeaderName:            "false",
			api.IsConsoleHeaderName:          "false",
			api.IsSmartTvHeaderName:          "false",
			api.IsTabletHeaderName:           "false",
			api.AdvertiserBrowserHeaderName:  "Firefox",
			api.AdvertiserDeviceOsHeaderName: "Win7",

			api.AdvertiserDeviceOsVersionHeaderName: "",
			api.DeviceOsVersionHeaderName:           "11.2",
			api.ResolutionHeightHeaderName:          "1920",
			api.ResolutionWidthHeaderName:           "1080",
		},
	}

	wurflData := &api.WurflData{
		DeviceType: api.DT_PC,
	}

	expInfo := &api.VisitorInfo{
		Uuid:           testUuid,
		AcceptLanguage: testAccLanguage,
		Language:       testLanguage,
		UserAgent:      testUa,
		Ip:             testIp,
		Country:        testLoc.Country,
		Country3:       testLoc.Country3,
		Region:         testLoc.Region,
		City:           testLoc.City,
		DeviceType:     api.DT_PC,
		Browser:        api.BROWSER_FIREFOX,
		Os:             api.OS_WINDOWS,
		NetSpeed:       testNetSpeed,
	}

	s.location.On("GetLocation", testIp).Return(testLoc).Once()
	defer s.location.AssertExpectations(s.T())

	s.wurfl.On("Parse", req.Headers).Return(wurflData).Once()
	defer s.wurfl.AssertExpectations(s.T())

	s.browscap.On("GetBrowser", testUa).Return(api.BROWSER_FIREFOX, nil).Once()
	s.browscap.On("GetOs", testUa).Return(api.OS_WINDOWS, nil).Once()
	defer s.browscap.AssertExpectations(s.T())

	s.carriers.On("Detect", testIp).Return(&api.Carrier{}, nil).Once()
	defer s.carriers.AssertExpectations(s.T())

	s.netSpeed.On("GetNetSpeed", testIp).Return(testNetSpeed, nil).Once()
	defer s.netSpeed.AssertExpectations(s.T())

	res, err := s.server.Detect(context.Background(), req)

	s.Require().NoError(err)
	s.Require().Equal(expInfo, res.VisitorInfo)
}

func (s *ServerTestSuite) TestDetectErrors() {
	testLoc := &api.Location{Country: "RU", Region: "RO", City: "rostov", Country3: "RUS"}
	testNetSpeed := uint32(api.NetSpeed_CABLE_DSL)

	req := &api.DetectRequest{
		Uuid: testUuid,
		Headers: map[string]string{
			api.XSocketIPHeaderName:          testIp,
			api.UserAgentHeaderName:          testUa,
			api.AcceptLangHeaderName:         testAccLanguage,
			api.IsDesktopHeaderName:          "true",
			api.IsPhoneHeaderName:            "false",
			api.IsConsoleHeaderName:          "false",
			api.IsSmartTvHeaderName:          "false",
			api.IsTabletHeaderName:           "false",
			api.AdvertiserBrowserHeaderName:  "Firefox",
			api.AdvertiserDeviceOsHeaderName: "Win7",

			api.AdvertiserDeviceOsVersionHeaderName: "",
			api.DeviceOsVersionHeaderName:           "11.2",
			api.ResolutionHeightHeaderName:          "1920",
			api.ResolutionWidthHeaderName:           "1080",
		},
	}

	wurflData := &api.WurflData{
		DeviceType: api.DT_OTHERS,
	}

	expInfo := &api.VisitorInfo{
		Uuid:           testUuid,
		AcceptLanguage: testAccLanguage,
		Language:       testLanguage,
		UserAgent:      testUa,
		Ip:             testIp,
		Country:        testLoc.Country,
		Country3:       testLoc.Country3,
		Region:         testLoc.Region,
		City:           testLoc.City,
		DeviceType:     api.DT_OTHERS,
	}

	s.location.On("GetLocation", testIp).Return(testLoc).Once()
	defer s.location.AssertExpectations(s.T())

	s.wurfl.On("Parse", req.Headers).Return(wurflData).Once()
	defer s.wurfl.AssertExpectations(s.T())

	s.browscap.On("GetBrowser", testUa).Return("", errors.New("internal err")).Once()
	s.browscap.On("GetOs", testUa).Return("", errors.New("internal err")).Once()
	defer s.browscap.AssertExpectations(s.T())

	s.carriers.On("Detect", testIp).Return(&api.Carrier{}, errors.New("internal err")).Once()
	defer s.carriers.AssertExpectations(s.T())

	s.netSpeed.On("GetNetSpeed", testIp).Return(testNetSpeed, errors.New("internal err")).Once()
	defer s.netSpeed.AssertExpectations(s.T())

	res, err := s.server.Detect(context.Background(), req)

	s.Require().NoError(err)
	s.Require().Equal(expInfo, res.VisitorInfo)
}

func (s *ServerTestSuite) TestDetectErrorIp() {
	req := &api.DetectRequest{
		Uuid: testUuid,
		Headers: map[string]string{
			api.AcceptLangHeaderName:         testAccLanguage,
			api.IsDesktopHeaderName:          "true",
			api.IsPhoneHeaderName:            "false",
			api.IsConsoleHeaderName:          "false",
			api.IsSmartTvHeaderName:          "false",
			api.IsTabletHeaderName:           "false",
			api.AdvertiserBrowserHeaderName:  "Firefox",
			api.AdvertiserDeviceOsHeaderName: "Win7",

			api.AdvertiserDeviceOsVersionHeaderName: "",
			api.DeviceOsVersionHeaderName:           "11.2",
			api.ResolutionHeightHeaderName:          "1920",
			api.ResolutionWidthHeaderName:           "1080",
		},
	}

	s.location.AssertNumberOfCalls(s.T(), "GetLocation", 0)
	s.wurfl.AssertNumberOfCalls(s.T(), "Parse", 0)
	s.browscap.AssertNumberOfCalls(s.T(), "GetBrowser", 0)
	s.browscap.AssertNumberOfCalls(s.T(), "GetOs", 0)
	s.carriers.AssertNumberOfCalls(s.T(), "Detect", 0)
	s.netSpeed.AssertNumberOfCalls(s.T(), "GetNetSpeed", 0)

	_, err := s.server.Detect(context.Background(), req)

	s.Require().Error(err)
	s.Require().Contains(err.Error(), "couldn't detect IP")
}

func TestServerTestSuite(t *testing.T) {
	suite.Run(t, new(ServerTestSuite))
}
