package server

import (
	"github.com/stretchr/testify/suite"
	"bitbucket.org/kovalevm/cup"
	"bitbucket.org/kovalevm/tools/mocks"
	"testing"
	"github.com/sirupsen/logrus/hooks/test"
	"google.golang.org/grpc"
	"bitbucket.org/kovalevm/ddsvc/api"
	"bitbucket.org/kovalevm/ddsvc/carriers"
	"bitbucket.org/kovalevm/ddsvc/browscap"
	"bitbucket.org/kovalevm/ddsvc/net_speed"
	"bitbucket.org/kovalevm/ddsvc/location"
	"bitbucket.org/kovalevm/ddsvc/wurfl"
)

type ProviderTestSuite struct {
	suite.Suite
	container cup.Container
	provider  *Provider
}

func (s *ProviderTestSuite) SetupTest() {
	s.container = cup.NewApp()
	s.container.Set("carriers.storage", func(c cup.Container) interface{} {
		return new(carriers.MockStorage)
	})

	s.container.Set("browscap.storage", func(c cup.Container) interface{} {
		return new(browscap.MockStorage)
	})

	s.container.Set("location.storage", func(c cup.Container) interface{} {
		return new(location.MockStorage)
	})

	s.container.Set("net_speed.storage", func(c cup.Container) interface{} {
		return new(net_speed.MockStorage)
	})

	s.container.Set("wurfl.storage", func(c cup.Container) interface{} {
		return new(wurfl.MockStorage)
	})

	s.container.Set("cache", func(c cup.Container) interface{} {
		return new(mocks.Cache)
	})
	s.container.Set("logger", func(c cup.Container) interface{} {
		logger, _ := test.NewNullLogger()

		return logger
	})
	s.container.Set("grpc.server", func(c cup.Container) interface{} {
		return grpc.NewServer()
	})

	s.provider = new(Provider)
}

func (s *ProviderTestSuite) TestRegisterServer() {
	s.provider.Register(s.container)

	pServer, err := s.container.Get("ddsvc.server")
	s.Require().NoError(err)
	s.Require().Implements((*api.DeviceDetectorServer)(nil), pServer)
}

func TestProviderTestSuite(t *testing.T) {
	suite.Run(t, new(ProviderTestSuite))
}
