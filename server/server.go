package server

import (
	"github.com/sirupsen/logrus"
	"bitbucket.org/kovalevm/ddsvc/carriers"
	"bitbucket.org/kovalevm/ddsvc/browscap"
	"context"
	"bitbucket.org/kovalevm/ddsvc/location"
	"bitbucket.org/kovalevm/ddsvc/api"
	"bitbucket.org/kovalevm/ddsvc/net_speed"
	"bitbucket.org/kovalevm/ddsvc/wurfl"
	"github.com/satori/go.uuid"
	"fmt"
	"bitbucket.org/kovalevm/tools/utils"
)

type server struct {
	carriers carriers.Storage
	browscap browscap.Storage
	location location.Storage
	netSpeed net_speed.Storage
	wurfl    wurfl.Storage
	logger   logrus.FieldLogger
}

func NewDeviceDetectorServer(
	carriers carriers.Storage,
	browscap browscap.Storage,
	location location.Storage,
	netSpeed net_speed.Storage,
	wurfl wurfl.Storage,
	logger logrus.FieldLogger) api.DeviceDetectorServer {

	return &server{
		carriers: carriers,
		browscap: browscap,
		location: location,
		netSpeed: netSpeed,
		wurfl:    wurfl,
		logger:   logger.WithField("service", "server"),
	}
}

func (s *server) Detect(ctx context.Context, req *api.DetectRequest) (*api.DetectResponse, error) {
	var (
		ok  bool
		err error
	)

	info := &api.VisitorInfo{}

	// set uuid
	if req.GetUuid() == "" {
		info.Uuid = uuid.NewV4().String()
	} else {
		info.Uuid = req.GetUuid()
	}
	info.Uuid = utils.SanitiseUtf8(info.Uuid)

	logger := s.logger.WithField("id", info.Uuid)
	logger.Debugf("Headers: %v", req.GetHeaders())

	// detect ip
	ip, err := detectIp(req.GetHeaders())
	if err != nil {
		return nil, fmt.Errorf("couldn't detect IP: %s", err)
	}
	info.Ip = ip

	// set Accept-Lang
	if info.AcceptLanguage, ok = req.GetHeaders()[api.AcceptLangHeaderName]; !ok {
		logger.Errorf("failed to find '%s' in headers", api.AcceptLangHeaderName)
	} else {
		if info.Language, err = detectLang(info.AcceptLanguage); err != nil {
			logger.Warnf("couldn't extract language from AcceptLanguage header: %s", err)
		}
	}

	// detect location
	loc := s.location.GetLocation(ip)
	info.Country = loc.Country
	info.Country3 = loc.Country3
	info.Region = loc.Region
	info.City = loc.City

	// detect carrier
	if carrier, err := s.carriers.Detect(ip); err != nil {
		logger.WithField("ip", ip).Errorf("Failed to detect carrier: %s", err)
	} else {
		info.Mcc = carrier.MCC
		info.Mnc = carrier.MNC
		info.Carrier = carrier.Carrier
	}

	// detect net speed
	if netSpeed, err := s.netSpeed.GetNetSpeed(ip); err == nil {
		info.NetSpeed = netSpeed
	} else {
		logger.WithField("ip", ip).Errorf("Failed to detect net speed: %s", err)
	}

	if ua, ok := req.GetHeaders()[api.UserAgentHeaderName]; ok {
		info.UserAgent = utils.SanitiseUtf8(ua)

		// parse wurfl headers
		wurflData := s.wurfl.Parse(req.GetHeaders())

		// set device type
		info.DeviceType = wurflData.DeviceType

		if info.DeviceType == api.DT_PC || info.DeviceType == api.DT_OTHERS {
			// if is PC use browscap for detect browser and OS
			if info.Browser, err = s.browscap.GetBrowser(ua); err != nil {
				logger.WithField("ua", ua).Errorf("Failed to detect browser via browscap: %s", err)
			}

			if info.Os, err = s.browscap.GetOs(ua); err != nil {
				logger.WithField("ua", ua).Errorf("Failed to detect os via browscap: %s", err)
			}
		} else {
			// if is not PC use wurfl for detect browser and OS
			info.Browser = wurflData.Browser
			info.Os = wurflData.Os
			info.OsVersion = wurflData.OsVersion
			info.ScreenWidth = wurflData.ScreenWidth
			info.ScreenHeight = wurflData.ScreenHeight
		}

		if info.DeviceType != api.DT_PC {
			info.Vendor = wurflData.Vendor
		}

		info.Device = wurflData.Device
	} else {
		logger.Errorf("failed to find '%s' in headers", api.UserAgentHeaderName)
	}

	logger.Debugf("Response: %+v", info)

	return &api.DetectResponse{VisitorInfo: info}, nil
}
