package net_speed

import (
	"github.com/stretchr/testify/mock"
)

type MockStorage struct {
	mock.Mock
}

func (s *MockStorage) Init() error {
	args := s.Called()

	return args.Error(0)
}
func (s *MockStorage) Shutdown() error {
	args := s.Called()

	return args.Error(0)
}

func (s *MockStorage) GetNetSpeed(ip string) (uint32, error) {
	args := s.Called(ip)

	return args.Get(0).(uint32), args.Error(1)
}
