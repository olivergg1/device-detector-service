package net_speed

import (
	"github.com/stretchr/testify/suite"
	"github.com/sirupsen/logrus"
	"github.com/sirupsen/logrus/hooks/test"
	"testing"
	"bitbucket.org/kovalevm/ddsvc/api"
)

type StorageTestSuite struct {
	suite.Suite
	logger     logrus.FieldLogger
	loggerHook *test.Hook
	storage    Storage
}

func (s *StorageTestSuite) SetupTest() {
	s.logger, s.loggerHook = test.NewNullLogger()
}

func (s *StorageTestSuite) TearDownTest() {
	s.storage.Shutdown()
}

func (s *StorageTestSuite) TestInit() {
	s.init()
}

func (s *StorageTestSuite) TestGetNetSpeed() {
	s.init()

	ip := "8.8.8.8"

	netSpeed, err := s.storage.GetNetSpeed(ip)
	s.Require().NoError(err)

	s.Require().Equal(netSpeed, uint32(api.NetSpeed_CORPORATE))
}

func (s *StorageTestSuite) TestGetNetSpeedBadIp() {
	s.init()

	ip := "very.bad.ip.123"

	_, err := s.storage.GetNetSpeed(ip)
	s.Require().Error(err)
	s.Require().Contains(err.Error(), "failed parse IP")
}

// If you have problem with this
// make sure that you have GeoIP2 Connection-Type database
// on your test machine. You can copy it from 'etc' folder in this repo
func (s *StorageTestSuite) init() {
	s.storage = NewStorage(s.logger, "/tmp/GeoIP2-Connection-Type.mmdb")

	if err := s.storage.Init(); err != nil {
		s.T().Fatalf("Failed init GeoIP2 Connection-Type storage, %v", err)
	}
}

func TestServerTestSuite(t *testing.T) {
	suite.Run(t, new(StorageTestSuite))
}
