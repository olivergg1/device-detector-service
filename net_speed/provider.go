package net_speed

import (
	"bitbucket.org/kovalevm/cup"
	"github.com/sirupsen/logrus"
	"os"
)

const netSpeedPathEnvName = "GEOIP_NET_SPEED_PATH"

type Provider struct{}

func (p *Provider) Register(c cup.Container) {
	p.registerPath(c)

	p.registerStorage(c)
}

func (p *Provider) Boot(c cup.Container) error {
	s := c.MustGet("net_speed.storage").(Storage)

	return s.Init()
}

func (p *Provider) Shutdown(c cup.Container) {
	s := c.MustGet("net_speed.storage").(Storage)
	logger := c.MustGet("logger").(logrus.FieldLogger)
	logger = logger.WithField("service", "net_speed")

	err := s.Shutdown()
	if err != nil {
		logger.Errorf("failed to shutdown net_speed storage, %s", err)
	}
}

func (p *Provider) registerStorage(c cup.Container) {
	c.Set("net_speed.storage", func(c cup.Container) interface{} {
		logger := c.MustGet("logger").(logrus.FieldLogger)
		path := c.MustGet("net_speed.path").(string)

		return NewStorage(logger, path)
	})
}

func (p *Provider) registerPath(c cup.Container) {
	c.Set("net_speed.path", func(c cup.Container) interface{} {
		logger := c.MustGet("logger").(logrus.FieldLogger)
		logger = logger.WithField("service", "net_speed")

		path, exist := os.LookupEnv(netSpeedPathEnvName)

		if exist && path != "" {
			if _, err := os.Stat(path); os.IsNotExist(err) {
				logger.Warnf("Found %s env variable, but file '%s' doesn't exist", netSpeedPathEnvName, path)
			} else {
				return path
			}
		}

		return ""
	})
}
