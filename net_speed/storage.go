package net_speed

import (
	"github.com/sirupsen/logrus"
	"fmt"
	"github.com/oschwald/geoip2-golang"
	"net"
)

const defaultPath = "/usr/share/GeoIP/GeoIP2-Connection-Type.mmdb"

type Storage interface {
	Init() error

	Shutdown() error

	GetNetSpeed(ip string) (uint32, error)
}

type storage struct {
	logger logrus.FieldLogger
	db     *geoip2.Reader
	path   string
}

func NewStorage(logger logrus.FieldLogger, path string) Storage {
	return &storage{
		logger: logger.WithField("service", "net_speed"),
		path:   path,
	}
}

func (s *storage) Init() error {
	var err error

	s.db, err = geoip2.Open(defaultPath)
	if err != nil {
		s.logger.Infof("Couldn't open Connection-Type db from default path - %s. %s", defaultPath, err)
		s.logger.Infof("Trying to open Connection-Type db from %s", netSpeedPathEnvName)

		if s.path == "" {
			return fmt.Errorf(
				"%s evn variable is empty and couldn't open Connection-Type from default path. %s",
				netSpeedPathEnvName,
				err,
			)
		}

		s.db, err = geoip2.Open(s.path)
		if err != nil {
			return fmt.Errorf("couldn't open Connection-Type from custom path. %s", err)
		}
	}

	if s.db == nil {
		return fmt.Errorf("internal error - db is nil")
	}

	return nil
}

func (s *storage) Shutdown() error {
	return s.db.Close()
}

func (s *storage) GetNetSpeed(ip string) (uint32, error) {
	parsedIp := net.ParseIP(ip)
	if parsedIp == nil {
		return 0, fmt.Errorf("failed parse IP - %s", ip)
	}

	ct, err := s.db.ConnectionType(parsedIp)
	if err != nil {
		return 0, err
	}

	result, ok := netSpeedMap[ct.ConnectionType]
	if !ok {
		return 0, fmt.Errorf("unknown net speed(connection type) - %s", ct.ConnectionType)
	}

	s.logger.WithField("ip", ip).Debugf("Net speed(connection type) is '%v'", result)

	return result, nil
}
