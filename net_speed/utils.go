package net_speed

import "bitbucket.org/kovalevm/ddsvc/api"

var netSpeedMap = map[string]uint32{
	"":          api.NetSpeed_UNKNOWN,
	"Dialup":    api.NetSpeed_DIALUP,
	"Cable/DSL": api.NetSpeed_CABLE_DSL,
	"Corporate": api.NetSpeed_CORPORATE,
	"Cellular":  api.NetSpeed_UNKNOWN, // because we don't have it in old GetIpNetSpeeds db
}
